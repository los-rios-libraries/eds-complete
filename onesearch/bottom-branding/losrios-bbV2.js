(function () { // move RTAC table to top. need to run this first, don't wait for jquery to load
	var rtacTable = document.querySelector('section.record-has-rtac');
	if (rtacTable) {
		var topArea = document.querySelector('.citation-wrapping-div');
		topArea.insertBefore(rtacTable, topArea.childNodes[0]);
	}
})();

(function() {
	// create global object containing all Los Rios-specific variables and functions
	window._LR = {
		pageType: (function() {
			if (location.href.indexOf('/basic') > -1) {
				return 'basic';
			} else if (location.href.indexOf('/results') > -1) {
				return 'result';
			} else if (location.href.indexOf('/detail/') > -1) {
				return 'detailedRecord';
			} else if (location.href.indexOf('/advanced') > -1) {
				return 'advanced';
			} else if(location.href.indexOf('/command') > -1) {
				return 'pubDetail';
			} else {
				return undefined;
			}
		}()),
		currentQuery: (function() {
			var searchLink = document.querySelector('.search-terms');
			var basicSearchField = document.getElementById('SearchTerm1');
			var advancedSearchField = document.getElementById('Searchbox1');
			var searchTerm;
			if (searchLink) {
				searchTerm = searchLink.innerHTML;
			} else if (basicSearchField) {
				searchTerm = basicSearchField.value;
			} else if (advancedSearchField) {
				searchTerm = advancedSearchField.value;
			}
			return searchTerm;

		}()),
		proxy: function(returnType) {
			var val;
			if (location.href.indexOf('.losrios.edu') > -1) {
				val = true;
			} else {
				val = false;
			}
			if (returnType === 'string') {
				val = val.toString();
			}
			return val;

		},
		guest: function(type) {
			var val;
			if (jQuery('.guest-login').length) {
				val = true;
			} else {
				val = false;
			}
			if (type === 'string') {
				val = val.toString();
			}
			console.log(val);
			return val;
		},
		framed: function(type) {
			var val;
			if (window.location !== window.parent.location) {
				val = true;
			} else {
				val = false;
			}
			if (type === 'string') {
				val = val.toString();
			}
			return val;
		},
		smallScreen: (function() {
			if (document.documentElement.clientWidth < 1024) {
				return true;
			}
			else {
				return false;
			}
			}()),
		fn: {
				waitForJQ: function(cb) {
					var retrieveJQ = setTimeout(function() { // if after 6 seconds jQuery has not been loaded, load it from remote source
						if (typeof jQuery === 'undefined') {
							_LR.fn.loadScript('//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js', false, 'head');
						}
					}, 6000);
					var jqCall = setInterval(function() { // ebsco warns that jquery may not be loaded initially
						if (typeof(jQuery) === 'function') {
							console.log('jquery found');
							clearTimeout(retrieveJQ);
							clearInterval(jqCall);
							console.log('timeout cleared');
							cb(jQuery);
						}
						else {
							console.log('no jQuery!');
						}
					}, 50);

				},
				parseHTML: function(str) { // EBSCO is using an old version of jQuery that does not include $.parseHTML so use this instead. REference: http://youmightnotneedjquery.com/#parse_html
				var tmp = document.implementation.createHTMLDocument('markup');
				tmp.body.innerHTML = str;
				return tmp.body.children;
			},
			setCookie: function(cname, cvalue, exdays, domain) {
				var expires; // set exdays to false for session-only cookie
				if (exdays !== false) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
					expires = ' expires=' + d.toUTCString() + ';';
				} else {
					expires = '';
				}
				document.cookie = cname + '=' + cvalue + ';' + expires + ' path=/; domain = ' + domain;
			},
			getCookie: function(cname) {
				var name = cname + "=";
				var ca = document.cookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') c = c.substring(1);
					if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
				}
				return "";
			},
			showLoader: function(el) {
				el.append('<img src="//www.library.losrios.edu/resources/databases/loader.gif" class="lr-loader" alt="loading">');
			},
			removeLoader: function() {
				jQuery('.lr-loader').remove();
			},
			showGBLink: function() { // this is called by detailed record widget if google book preview is found. JS parsers show a warning because it's not called on this page
				if (typeof(jQuery) === 'function') { // unlikely this would be fired before jquery loads, but just in case
					var $ = jQuery;
					var button = $('<a />').attr({
						'href': '#BookPreview',
						'id': 'gbButton'
					}).html('<img src="' + _LR.servers.domain + _LR.servers.root + '/onesearch/bottom-branding/gbs_preview_button.gif" alt="Google Preview">');
					button.hide().insertAfter($('[data-auto="citation_title"]')).fadeIn().removeAttr('style'); // show button jus tafter item title
					$('#gbButton').on('click', function(e) {
						e.preventDefault();
						ga('send', 'event', 'gbooks preview button', 'click');
						$('html, body').animate({ // smooth scroll to element. Not perfect but best I could find...
							scrollTop: $("#BookPreview").offset().top
						}, 400);
					});
				}
			},
			fixPermalink: function(str) {
				var fix = str.replace('https://ezproxy.losrios.edu/login?url=', '').replace('direct=true', 'authtype=ip,guest&custid=' + _LR.custID + '&groupid=main&profile=eds&direct=true');
				_LR.permalink = fix;
				return fix;
			},
			loadScript: function(filename, ours, el) {
				// load additional scripts depending on page type
				var a = document.createElement('script');
				var scName = filename.slice(-4) + 'LRjs';
				if (!(document.getElementById(scName))) {
					var url;
					if (ours === true) {

						url = _LR.servers.domain + _LR.servers.root + '/onesearch/' + filename + _LR.servers.extension;

					} else { // for remote scripts such as ebsco's for pub finder alpha browse
						url = filename;
					}
					a.src = url;
					a.id = scName;
					a.async = false;

					el = el || 'body';

					document.getElementsByTagName(el)[0].appendChild(a);
					if (typeof(_LR.loadedScripts) !== 'object') {
						_LR.loadedScripts = [];
					}
					_LR.loadedScripts.push(scName);
				}
			},
			getMetadata: function(label) { // for getting OCLC and ISBNs on detailed records pages, could be used elsewhere too
				var a = jQuery('#citationFields dt');
				var v = '';
				a.each(function() {
					if ($(this).text() === label + ':') {
						$(this).data('label', label);
						v = $(this).next().text();
						return false;
					} else {
						return true;
					}

				});
				return v;
			},
			getResultItemData: function(el) { // used for e.g. doi lookup
				var link = ''; // get customlink URL, used to get Kanopy images
				var customLink = el.find('.ils-link:first-of-type');
				if (customLink.length) {
					var target = customLink.attr('href');
					link = decodeURIComponent(target);
				}
				
				var info = el.find('em.preview-hover').data('hoverpreviewjson');
				if (info !== undefined) {
					return {
						db: info.db,
						an: info.term,
						url: link
					};
					//           console.log('Result ' + (i + 1) + ': db is ' + db + '  and AN is ' + an);

				} else {
					return {
						db: '',
						an: '',
						url: link
					};
				}



			},
			getDOI: function(db, an, container) { // get doi from api. This is better than getting it from page because many records do not show it on the page but the info is there in the API.
				var $ = jQuery;
				     console.log('getDOI running');
					 var pmid_to_doi = function() { // use Pubmed API to get doi from PMID
						if (db === 'cmedm') {
							var pmid = an; 
							$.getJSON('https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?ids=' + pmid + '&tool=ebscodiscoveryservice&email=KarlseJ%40scc.losrios.edu&format=json')
							.done(function(r) {
								if (r.status === 'ok') {
									var doi = r.records[0].doi;
									if (doi) {
										_LR.fn.getOADoi(doi, container);
										
									}
									
								}
								})
								.fail(function(a,b,c) {
									ga('send', 'event', 'pubmed doi query', 'error', c);
								});
						}
					};
				if (db !== '') {
					var doi = '';
					var URL = 'https://widgets.ebscohost.com/prod/encryptedkey/eds/eds.php?k=' + _LR.currentCol.edsGWTwo + '&s=0,1,1,0,0,0&q=';
					var query = 'retrieve?dbid=' + db + '&an=' + an;
					var retrieveURL = URL + encodeURIComponent(query);
					$.getJSON(retrieveURL)
						.done(function(data) {
							if (data.Record)
							{
								var identifiers = data.Record.RecordInfo.BibRecord.BibEntity.Identifiers;
								if (identifiers)
								{
									var hasDOI = false;
									$.each(identifiers, function(i)
									{
										if (identifiers[i].Type === 'doi')
										{
											hasDOI = true;
											doi = identifiers[i].Value;
											//                      console.log(doi);
											_LR.fn.getOADoi(doi, container);
										}
									});
									if (hasDOI === false) {
										pmid_to_doi();

									}
								}
								else {
									pmid_to_doi();
								}
							}
						})
						.fail(function(a, b, c) {
							ga('send', 'event', 'eds api', 'error', c);
							console.log('eds api lookup failed, error: ' + c);
						});

				}
			},
			getOADoi: function(doi, container) { // use oadoi api to look for free full text.
				if (doi !== '') {
					var email = 'karlsej@scc.losrios.edu'; // supposed to append this to query url to help them track usage
					var apiURL = 'https://api.unpaywall.org/v2/' + doi + '?email=' + email;
					jQuery.getJSON(apiURL).done(function(data) {
						var isOA = data.best_oa_location;
						//         console.log('oadoi url: ' + url);
						if (isOA !== null) {
							console.log('OA doi found');
							_LR.fn.appendOALink(isOA.url, container);
						}

					}).fail(function(a, b, c) {
						ga('send', 'event', 'oadoi link', 'error', c);
					})
					.always(function() {
						ga('send', 'event', 'unpaywall api', 'call');
					});
				}
			},
			appendOALink: function(url, container) {
				//     console.log(container);
				var el;
				if (container.hasClass('display-info')) {
					el = jQuery('<div />');
				} else if (container.hasClass('format-control')) {
					el = jQuery('<li />');
				}
				else if (container.hasClass('externalLinks')) {
					el = jQuery('<span />');
				}
				//     console.log(el);
				el.css({border: 'none', display:'inline'}).addClass('custom-link-item').html('<span class="custom-link"><a onclick="ga(\'send\', \'event\', \'oadoi link\', \'click\');" target="_blank" href="' + url + '"><img class="icon-image" src="//www.library.losrios.edu/resources/link-icons/oa.png" alt="">View Full Text (open access)</a></span>').appendTo(container);

			},
			fixArtstorUrl: function(url) { // fix needed for thumbnails in Artstor URLs - can remove if/when EBSCO fixes
				var newUrl = url.replace('media.artstor.net', 'mdxdv.artstor.org/thumb');
				return '<img alt="" src="' + newUrl + '">';
				
			}
		},
		circulating: /Circulating|Juvenile|Easy|Children|Popular|Plays|SCC-3rd Floor|New Book|Display Books|Success|ARC-DVDs-1st Floor|FLC-Media/
	};

}());
(function () {
	var arr = ep.clientData.pid.split('.');
	_LR.custID = arr[0];
	_LR.group = arr[1];
	_LR.profile = arr[2];
	_LR.profType = (function () { // used primarily to route test profiles to development script files
		if (ep.clientData.pid.indexOf('sandbox') > -1) { // string sandbox must be in either the group name or profile name in EBSCOadmin
			return 'test';
		}
		else {
			return 'production';
		}
	}());
	if (_LR.proxy() === true) {
		_LR.domain = 'losrios.edu';
	}
	else {
		_LR.domain = 'ebscohost.com';
	}

}());

(function() {
	var lr = '.losrios.edu/';
	var homes = [
				 'http://www.arc' + lr + 'student-resources/library/',
				 'https://www.crc' + lr + 'student-resources/library/',
				 'http://www.flc' + lr + 'student-resources/library/',
				 'https://www.scc' + lr + 'library/'
				 ];
	_LR.colProps = [
		// all variables that differ by college should go in this object
		{
			'arc': {
				colName: 'American River College',
				abbr: 'arc',
				custID: 'amerriv',
				zipFrag: '41',
				homePage : homes[0],
				researchHelpPage: homes[0] + 'contact-the-library',
				reservesURL: homes[0] + 'textbooks',
				galeID: 'sacr22807',
				edsGateway: 'eyJjdCI6IklkeVwvdVdORUxSbEVab1hqVGFQYlwvWHJTdDdQeU5OYk9xdjFxRld5Z2Zpdz0iLCJpdiI6IjhjOTgyMGMzMDBiYTE3MmMyZGFlMTAwZjMwNDBkYmQ3IiwicyI6ImQxN2EyY2Y0OWEzMjZjNmUifQ==&p=YW1lcnJpdi5tYWluLmd2cmxfYXBp',
				edsGWTwo: 'eyJjdCI6Ik4wMmx2ZURYYk1tNVdMZ1hPVklyWWpneEdjVFJLemFEOTNJVTk1MVY4bDA9IiwiaXYiOiJlODIzZjhmNGM3NjAzMDBjZGQyMDdjODk0NWFkMzhhOSIsInMiOiJhMmM1NGE4MzQ0OGIyYTllIn0=&p=YW1lcnJpdi5tYWluLndzYXBp',
				askWidget: '.libraryh3lp'
			}
		},
		{
			'crc': {
				colName: 'Cosumnes River College',
				abbr: 'crc',
				custID: 'cosum',
				zipFrag: '23',
				homePage: homes[1],
				researchHelpPage: homes[1] + '/contact/email-a-librarian-service',
				reservesURL: homes[1] + 'textbooks',
				galeID: 'sacr73031',
				edsGateway: 'eyJjdCI6Ims2eGVPMlVSY1hSd1pRNSswbWU2TGxMSGNHQk1DTGVSaFE5SFU0V1pScUE9IiwiaXYiOiIxZTUxNjkyNGNmNDYxYmQ4OWM5ODJiNDQyMjJhNGExMSIsInMiOiIxZmY1OWFmMzIxNTViZmRiIn0=&p=Y29zdW0ubWFpbi5ndnJsX2FwaQ==',
				edsGWTwo: 'eyJjdCI6IndtM21pVXJodHNwSCs1eDMweWxvNmNFYmVrRU4rVXNTRFJuVlIyMjN5Y2c9IiwiaXYiOiI0YTBhOTY0ZWMyOGJiNzc4M2FhMTg5MzU3ZjM0MzkwYyIsInMiOiJlMjA3ZmY1YTFmN2YwOTY5In0=&p=Y29zdW0ubWFpbi53c2FwaQ==',
				askWidget: '.jx_ui_Widget, .zopim'
			}
		},
		{
			'flc': {
				colName: 'Folsom Lake College',
				custID: 'ns015092',
				abbr: 'flc',
				zipFrag: '30',
				homePage: homes[2],
				researchHelpPage: homes[2] + 'contact',
				reservesURL: homes[2] + 'textbooks',
				galeID: 'sacr88293',
				edsGateway: 'eyJjdCI6Ikk4VkZnc25aMU0xSjlUZGgzTFwvcGkyc1l4ZFQycXJGVG1rdWlhU1NHWGFJPSIsIml2IjoiNTFmMDdiZDlhZjZlZTA0OWE3MjIxODg3NWQ2YmI2NjkiLCJzIjoiMzVkMTdhMmMyMTdhNGQ2YSJ9&p=bnMwMTUwOTIubWFpbi5ndnJsX2FwaQ==',
				edsGWTwo: 'eyJjdCI6ImhSSWtJR3ZVQ1AzTzNLejRTYzN0OHA4ejNWdFlzRFlXRTE2VnJjRGZlKzQ9IiwiaXYiOiI4Y2Y3ZWY5MzFlODNjMDBlMDNiYzM3NGI4ZWY4NDNjYSIsInMiOiIyZWRmZjkwOWJlYzdkMzVlIn0=&p=bnMwMTUwOTEubWFpbi53c2FwaQ=='
			}
		},
		{
			'scc': {
				colName: 'Sacramento City College',
				abbr: 'scc',
				custID: 'sacram',
				zipFrag: '22',
				homePage: homes[3],
				researchHelpPage: homes[3] + 'services/ask-librarian',
				reservesURL: homes[3] + 'services/textbooks-on-reserve/',
				galeID: 'cclc_sac',
				edsGateway: 'eyJjdCI6ImVZQ1ViUWFrTzZ2OWZEb0g4YVRob1k0dGdqNmxZbXZcL2xvTW4rWW9kZzBJPSIsIml2IjoiYzRiNzU3YTQwNWQ0NGFmYjkwOTE0NjA5NDJmY2VkNjkiLCJzIjoiNmRlYjk0MjRmNWY4MDM5ZCJ9&p=c2FjcmFtLm1haW4uZ3ZybF9hcGk=',
				edsGWTwo: 'eyJjdCI6ImFkdWw0Y0VUM0FyT1wvaGlJUSsxeFR3REdTcVQxdURwZkVtd2NZWmxER1wvUT0iLCJpdiI6ImVjOGIyYzBiNzUzYzc1YzJiMGE1ZmVjMzFkM2JmZWM0IiwicyI6ImMxOWQ2MDViZDFjYzliYTcifQ==&p=c2FjcmFtLm1haW4ud3NhcGk=',
				askWidget: '.libchat-block'
			}
		}

	];

	_LR.currentCol = (function() { // set the current college and allow us to use object syntax
		var cust = _LR.custID;
		for (var i = 0; i < _LR.colProps.length; i++) {

			for (var key in _LR.colProps[i]) {

				if (_LR.colProps[i].hasOwnProperty(key)) {
					if (_LR.colProps[i][key].custID === cust) {
						_LR.college = _LR.colProps[i][key].abbr;
						return _LR.colProps[i][key];
					}
				}
			}

		}
	}());
	_LR.primoRoot = 'https://caccl-lrccd.primo.exlibrisgroup.com/discovery/';
	_LR.primoView = 'vid=01CACCL_LRCCD:' + _LR.currentCol.abbr;
	_LR.primoUrlBase = _LR.primoRoot + 'search?' + _LR.primoView;
	console.log(_LR.currentCol);
	_LR.servers = {
		domain: '//www.library.losrios.edu/',
		root: (function(type) {
			if (type === 'production') {
				return 'resources';

			} else if (type === 'test') {
				return _LR.college + '/bitbucket/eds-complete';
			} else {
				return '';
			}
		}(_LR.profType)),
		subRoot: (function(type) { // for files outside of eds-complete
			if (type === 'production') {
				return 'resources';

			} else if (type === 'test') {
				return _LR.college + '/bitbucket';
			} else {
				return '';
			}
		}(_LR.profType)),
		extension: (function() {
			if (_LR.profType === 'production') {
				return '.min.js';

			} else if (_LR.profType === 'test') {
				return '.js';
			} else {
				return '';
			}
		}())
	};
	// show note when there are problems
	_LR.fn.showNote = (function(fileName) {
		if (this.getCookie(fileName) !== 'hide') {
			var url = _LR.servers.domain + _LR.servers.root + '/onesearch/bottom-branding/' + fileName + '.php'; // this file needs to be edited with current message
			$.get(url)
				.done(function(response) {
					if (response !== '') {
						$('<p id="problem-note" style="display:none;">' + response + '<button id="problem-note-dismiss" class="button" type="button">Hide this message</button></p>').prependTo('#header').slideDown('slow');
						$('#problem-note-dismiss').on('click', function() {
							$(this).parent().slideUp();
							_LR.fn.setCookie(fileName, 'hide', false, _LR.domain);
						});
					}
				})
				.fail(function(a, b, c) {
					ga('send', 'event', 'problem note', 'error', c);
				});
		}
	});
	_LR.fn.getResultItemData.isbn = (function(el, callback) {
		var URL = 'https://widgets.ebscohost.com/prod/encryptedkey/eds/eds.php?k=' + _LR.currentCol.edsGWTwo + '&s=0,1,1,0,0,0&q=';
		var query = 'retrieve?dbid=' + _LR.fn.getResultItemData(el).db + '&an=' + _LR.fn.getResultItemData(el).an;
		var retrieveURL = URL + encodeURIComponent(query);
		$.getJSON(retrieveURL)
			.done(function(data) {
				console.log(data);
				var identifiers = data.Record.Items;
				if (identifiers) {
					$.each(identifiers, function(i) {
						if (identifiers[i].Name === 'ISBN') {
							var isbns = identifiers[i].Data.split(' '); 
							var isbnFirst = isbns[0].split('&');
							var isbn = isbnFirst[0];
							if (isbn !== '') {
								callback(isbn);
							}
						}

					});
				}
			})
			.fail(function(a, b, c) {
				ga('send', 'event', 'eds api', 'error', c);
			});
	});

}());




(function () {
	// so here we have it, wait until jQuery is loaded and then load the remaining scripts.
	_LR.fn.waitForJQ(function($) {
			(function() { // hide full text online and available in library collection limiters on basic search and results screens
				if (location.href.indexOf('advanced?') === -1) {
					var a = $('#common_FT, #common_FT1');
					if (a.length) {
						a.parents('.limiter-item').hide();
						a.parents('li').hide();
					}
					var b = $('.selected-limiters .limiter');
					if (b.length) {
						b.each(function() {
							if (/Full Text|Only Show/.test($(this).text())) {
								//if ($(this).text().indexOf('Full Text') === 0) {
								$(this).parents('li').hide();
							}
						});
						// hide limiters heading in breadbox if no visible limiters
						var c = $('.bb-heading:contains("Limiters")');
						if (c.length) {
							var d = c.closest('.selected-limiters');
							if (d.find('li:visible').length === 0) {
								d.hide();
							}
						}
					}
				}

			})();
			
			// begin script loads
			if (_LR.pageType === 'result') {
				_LR.fn.loadScript('results/eds-results', true);
				//  _LR.fn.loadScript('results/subjects', true);
			}
			if ((_LR.pageType !== 'detailedRecord') && (_LR.pageType !== 'pubDetail')) {
				// search suggestions need to be on search, non-results screens as well because when there are no results, that's where you end up.
				if (_LR.currentQuery !== '') {
					if (_LR.currentQuery.match(/^(DE |AU |MM |SU |TX |TI )/) === null) {
						_LR.fn.loadScript('bottom-branding/search-tips', true);
					}
				}
				if (_LR.pageType === 'advanced') {
					(function() { // increase size of location select box and auto-select catalog only limiter when someone uses it
						var a = $('#common_FC');
						$('#common_LB').attr('size', '11').find('option').on('click', function() {
							if (a.prop('checked') !== true) {
								a.prop('checked', true);
							}
						});
					}());

				}
			} else if (_LR.pageType === 'pubDetail') { // links to publisher URLs open by default in iframes, which is bad UX and is often broken in SSL environment, so here is a workaround.
				var pubLink = $('#publisherInfo a');
				pubLink.attr({
					href: pubLink.attr('title'),
					target: '_blank'
					});
				
			}
			
			else { // for detailed record pages--originally there were not so many scripts here, may need to move to separate one?
				(function() {
						_LR.itemData = {
							// get item elements used elsewhere
							db: function(el) {
								if (ep.clientData.currentRecord) {
									return ep.clientData.currentRecord.Db;
								} else {
									return '';
								}
							},
							an: function(el) {
								if (ep.clientData.currentRecord) {
									return ep.clientData.currentRecord.Term;
								} else {
									return '';
								}
							},
							isbn: function() {
								if ($('#citationFields').length) {
									var isbnDT = $('#citationFields dt:contains("ISBN")');
									var finalISBN = '';
									var isbnStr = '';
									if (isbnDT.length) {
										isbnStr = isbnDT.next().html();
									}
									if (isbnStr !== '') {
										var arr = isbnStr.split('<');
										var nextArr = arr[0].split(' ');
										finalISBN = nextArr[0];
									}
									return finalISBN;

								} else {
									return '';
								}

							},
							oclc: function() {
	
									var rawOC = _LR.fn.getMetadata('OCLC');
									var oclcNo = rawOC.replace('ocm', '');
									return oclcNo;
							}

						};
					// rewrite permalinks only for our own local content
					if (/^cat01/.test(_LR.itemData.db()) !== false) {
						ep.clientData.plink = _LR.fn.fixPermalink(ep.clientData.plink);
					}

					(function() {
						// permalinks on detailed record pages
						$('.citation-title').after(
							'<dt>Permalink:</dt><dd id="lr-permalink-dd"><input id="lr-input-permalink" type="text" value="' + ep.clientData.plink + '"> <button id="lr-copy-permalink" type="button" class="button ">&#128279; Copy URL</button></dd>'

						);

						var input = $('#lr-input-permalink');
						input.on('click', function() {
							this.select();
							ga('send', 'event', 'permalink feature', 'copy', 'input box');
						});
						if (typeof(document.execCommand) === 'function') { // make sure browser is able to copy to clipboard

							$('#lr-copy-permalink').show().on('click', function() {

								input.select();
								document.execCommand('Copy');
								input.blur();
								if (!($('#lr-permalink-copied').length)) {
									$('body').append('<div id="lr-permalink-copied" >Link copied to clipboard</div>');
								}
								var permCopied = $('#lr-permalink-copied');
								permCopied.dialog({
									create: function() {
										permCopied.parent().attr('id', 'lr-permalink-dialog');
									},
									position: {
										my: 'right bottom',
										at: 'right top',
										of: $('#lr-copy-permalink')
									},
									minHeight: 20,
									hide: {
										effect: 'fadeOut'
									},
									show: {
										effect: 'fadeIn'
									},
									open: function() {
										setTimeout(function() {
											permCopied.dialog('close');
										
										}, 5000);

									}

								});
								ga('send', 'event', 'permalink feature', 'copy', 'button');

							});
						}



					}());
					// add problem report button
					if ($('.format-control').length) {
						$('#Column1Content').append('<button class="button" type="button" id="lr-problem-reporter">Report a problem</button>');
						$('#lr-problem-reporter').on('click', function () {
							var w = 600;
							var h = 600;
							var left = (screen.width - w) / 2;
							var top = (screen.height - h) / 4;
							var refUrl = ep.clientData.plink || '';
							var itemID = ep.clientData.currentRecord.Term || '';
							window.open('https://www.library.losrios.edu/utilities/problem-reporter/?url=' + encodeURIComponent(refUrl) + '&recordid=' + itemID + '&college=' + _LR.currentCol.abbr + '&source=ebsco', 'Problem reporter', 'toolbar=no, location=no, menubar=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);


						});
					}
					// include content in subject line for emails
					_LR.fn.loadSubj = function(selector)
					{
						var title = $('h1').html();
						title = title.replace(/ \/ .*/, '').replace (' :', ':'); // get rid of statement of responsibility and extra spaces in catalog items
						var arr = title.split(' ');
						var end = '';
						var limit = 8; // max number of words before subject line gets truncated
						if (arr.length > limit)
						{
							end = '...';
							arr = arr.slice(0, limit + 1);
						}
						var emailSubj = arr.join(' ') + end + ' - OneSearch';
						var wait = setInterval(function()
						{
							if ($(selector).length)
							{
								clearInterval(wait);
								// email issues february 2019
								/*
								if (ep.clientData.currentRecord.Db !== 'cat01047a') {
									var message = '<p id="lr-email-warning"><strong>Warning: some users are reporting problems receiving articles emailed from OneSearch. Consider saving articles to your computer instead. (February 22, 2019, 8:55 am)</strong></p>';
									$('.panel-header').after(message);
								}
								*/
								// end email issues - comment out when resolved
								
								$(selector).val(emailSubj);
							}


						}, 100);
					};
					$('.email-link').on('click', function() {
						_LR.fn.loadSubj('#DeliveryEmailSubject');		
					});
										if (_LR.itemData.db() === 'edsart') {
						var el = $('#bookJacket');
						var url = el.data('book-jacket').ThumbImageUrl;
						el.append(_LR.fn.fixArtstorUrl(url));
					}
					else if (/edskan|edsfod/.test(_LR.itemData.db())) {
						(function() {
							if (!($('#bookJacket').length))
							{
								var url = '';
								var arr = (_LR.itemData.an().split('.'));
								if (arr[0] === 'edsfod') {
									url = 'https://fod.infobase.com/image/' + arr[1];
								}
								else if (arr[0] === 'edskan') {
									url = 'https://www.kanopy.com/node/' + arr[1] + '/external-image';
								}
								var markup = '<dt class="hidden-access">Video Image: </dt><dd class="citation-image-box"><div id="bookJacket"><img src="' + url + '" alt="Video image" onerror="$(\'.citation-image-box\').remove();"> </div></dd>';
								if ($('#lr-permalink-dd').length)
								{ // since we added this feature, image needs to go after it - should load before this script does
									$('#lr-permalink-dd').after(markup);
									}
									else
									{
										$('.citation-title').after(markup);
									}
								}
							
							}());
						
					}
					// check if catalog record
					if (ep.clientData.currentRecord.Db.indexOf('cat0') === 0) { // both custom catalogs begin with this string
						_LR.fn.loadScript('detailed-record/opac-scripts', true);
					}
					
					if ((!($('.pdf-ft').length)) && (!($('.html-ft').length)))
					{
						var article = false; // might try to combone next conditional into function
						var pubType = $('dt:contains("Publication Type")');
						var docType = $('dt:contains("Document Type")');
						if (pubType.length)
						{
							if (/article|journal/i.test(pubType.next().text()))
							{
								article = true;

							}
						}
						else if (docType.length)
						{
							if (docType.next().text().indexOf('Article') > -1)
							{
								article = true;
							}

						}

						if ((article === true) || (ep.clientData.currentRecord.Db === 'cmedm'))
						{
							var count = 0;
							$('.ils-link').each(function()
							{
								if (/^full text/i.test($(this).text()))
								{
									count++;
								}
							});
							if (count === 0)
							{
								_LR.fn.getDOI(_LR.itemData.db(), _LR.itemData.an(), $('.format-control:first-of-type'));
							}

						}
					}
					if (_LR.itemData.isbn() !== '') {
						setTimeout(function() {
							if (!($('#bookJacket img').length)) { // if generic cover is shown, check open library for cover
								var isbn = _LR.itemData.isbn();
								var openLibImg = '<img src="//covers.openlibrary.org/b/isbn/' + isbn + '-M.jpg" id="bj-' + isbn + '" alt="Book Jacket"  style="position:absolute;left:-9999px;">';
								$('body').append(openLibImg);
								$('#bj-' + isbn).on('load', function() { // api returns 1px image if no match
									if ($(this).height() > 2) {
										$('#lr-permalink-dd').after('<dd class="citation-image-box"><div id="bookJacket" data-auto="citation_book_jacket"><div style="height: auto;"></div> </div></dd>');
										$(this).appendTo($('#bookJacket')).removeAttr('style');
									}
								});
							}
						}, 2800);
					}
					// hide navigation area if user has arrived via permalink
					setTimeout(function() {
					if (ep.getUrlParam('vid') === '0') {
						$('.content-header').hide();
					}
					},800);
					$('#citationFields a').each(function() { // rewrite proxied links to use SPU structure
						var proxyStr = '.ezproxy.losrios.edu';
						var a = $(this);
						if (a.text().indexOf(proxyStr) > -1) {
							var arr = a.text().split(proxyStr);
							var first = arr[0].replace(/\-/g, '.');
							newStr = 'https://ezproxy.losrios.edu/login?url=' + first + arr[1];
							a.attr({
								href: newStr,
								title: ''
							}).text(newStr);
						}
					});
					// GA events for detailed record only
					$('.rtac-show-more, .rtac-show-less').on('click', function() {
						ga('send', 'event', 'holdings table', 'toggle', 'show more, show less');
					});
					$('.article-tools').on('click', 'a', function() { // easybib ones are now generated dynamically
						var label = $(this).text();
						ga('send', 'event', 'tools', 'select', label);
					});
					$('.book-details a').on('click', function() {
						var label = $(this).text();
						ga('send', 'event', 'book links', 'click', label);
					});
					$('.custom-widget-header a').on('click', function() {
						var widgetTitle = $(this).children('span.text').text();
						ga('send', 'event', 'detailed-record widgets', 'toggle', widgetTitle);
					});
				}());
			}


			(function () { // redirect EDS searches to Primo VE
				if (_LR.pageType === 'basic' || 'advanced') {
					var errorSpan = $('#UserMessageLabel');
					var regex = /AN lr(d|ois)\.b/;
					var an = _LR.currentQuery.replace(regex, 'b');
					var primoUrl = _LR.primoUrlBase + '&query=any,contains,' + an + '&search_scope=' + _LR.currentCol.abbr + '_combined&tab=combined';
					var message = '<p class="search-messaging">This title should be findable <a href="' + primoUrl + '">in the new OneSearch</a>. If it does not appear, please contact your Library for further assistance.</p>';
					if (errorSpan.length) {
						if (errorSpan.html().indexOf('No results were found') > -1) {
							if (regex.test(_LR.currentQuery)) {
								var cookieVal = encodeURIComponent(_LR.currentQuery);
								var prevUse = _LR.fn.getCookie('edsLOISPerm');
								errorSpan.after(message);
								if (prevUse !== cookieVal) {
									_LR.fn.setCookie('edsLOISPerm', cookieVal, false, _LR.domain);
									ga('send', 'event', 'Sierra permalink', 'redirect', cookieVal);
									location.href = primoUrl;
								}

							}
						}
					} else if (regex.test(_LR.currentQuery)) {
						errorSpan.after(message);

					}

				}

			}());

			(function () {
				// rewrite permalinks on results pages
				var a;
				if (_LR.pageType === 'result') {
					var el = $('#pLink');
					if (el.length) {
						a = _LR.fn.fixPermalink(el.val());
						$('#pLink').val(a);
					}
                                        else {
                                          a = 'http://search.ebscohost.com/login.aspx?authtype=ip,guest&custid=' + _LR.custID + '&group=main&profile=' + _LR.profile + '&direct=true&bquery=' + encodeURIComponent(_LR.currentQuery);
                                          _LR.permalink = a;
                                        }

				}
				else if (_LR.pageType === 'basic') {
					_LR.permalink = 'http://search.ebscohost.com/login.aspx?authtype=ip,guest&custid=' + _LR.custID + '&groupid=' + _LR.group + '&profile=eds';
				}
				$('.title-link, .citation-title a').each(function () {
					$(this).html($(this).html().replace(/\[electronic resource\]|\[videorecording\]/i, ''));

				});

				_LR.fn.setCookie('homeLibrary', _LR.college, 30, _LR.domain); // set hoome library cookie. this helps with databases page and perhaps other things
				// have databases  and my record page open in same window. If order of links changes this will need to change

				$('#Toolbar li a').removeAttr('target');


				// load alphabetical title browse on pub finder screens
				var pubFind = document.querySelector('.searching');
				if (pubFind) {
					if (pubFind.innerHTML.indexOf('Periodicals by Title') > -1) {
						_LR.fn.loadScript('//widgets.ebscohost.com/prod/common/branding/pubfinder-title-browse.js', false);
						//						(function() {
						//	function searchQuery(s) {
						//		var q = ep.newReturnUrl.split('?')[1] || false;
						//		var cr = ep.clientData.currentRecord || false;
						//		if (cr) { for (var key in cr) { if (s === key || s === cr[key]) return true; } }
						//		if (q) { if (q.indexOf('&bdata=') > -1) { var w = ''; q = q.split('&bdata=');
						//		for (var k = 1; k < q.length; k++) { w += atob(q[k]); } q = q[0] + w; }
						//		q = q.split('&'); for (var i = 0; i < q.length; ++i) { var b = q[i].split('=');
						//		if (s === b[0] || s === b[1]) return true; } } return false; }
						//		if (searchQuery('edspub')) {
						//			var p = '{"type":[{"label":"Periodicals Only","pt":" AND (ZT Journal OR ZT Newsletter OR ZT Report OR ZT Proceedings OR ZT Newspaper)","d":true},{"label":"All resources","pt":" ","d":false}]}';
						//			var pfiLimiter = document.createElement('script');
						//			pfiLimiter.async = true;
						//			pfiLimiter.src = _LR.servers.domain + _LR.servers.root + '/onesearch/edspub/pubfinder' + _LR.servers.extension;
						//			pfiLimiter.id = 'pubtypesearch-app';
						//			pfiLimiter.setAttribute('data-p', encodeURI(p));
						//			parent.document.body.appendChild(pfiLimiter);
						//		}
						//}());
					}
				}


/*
				// get update note
				$.ajax({
						url: _LR.servers.domain + _LR.servers.root + '/onesearch/bottom-branding/cat-update/update-note.php'
					})
					.done(function (data) {
						$('#report-a-problem').after('<p id="cat-update-note">' + data + '</p>');
					})
					.fail(function (jqXHR, textStatus, errorThrown) {
						ga('send', 'event', 'cat update note', 'error', errorThrown);

					});
*/

				// GA opt out
				$('.branding-container').append('<p id="opt-area">The Library uses Google Analytics to track activity on this site. <span id="ga-status"><a target="_blank" id="ga-opt-out" href="'+ _LR.servers.domain + _LR.servers.subRoot + '/notes/google-analytics.php?source=onesearch">Read more</a></span><button type="button" id="opt-out">Opt out</button></p>');
				var optStatus = _LR.fn.getCookie('lrGAOptOut');
				if (optStatus === 'y') {
					$('#ga-status').prepend(' You are opted out. ');
					$('#opt-out').remove();
					$('#opt-area').append('<p>Want to help us evaluate the usage of this site?</p><button type="button" id="opt-in">Opt in</button>');
					$('#opt-in').on('click', function () {
						_LR.fn.setCookie('lrGAOptOut', 'n', 1, '.' + _LR.domain);
						$(this).html('Opted in').addClass('disabled').after(' <em>Thanks!<em>');
					});
				}
				$('#ga-opt-out').on('click', function (e) {
					e.preventDefault();
					var optPage = $(this).attr('href');
					$.get(optPage)
					.done(function(d) {
						var page = $(d);
						var body = page.find('#ga-note');
						body.dialog({
							modal: true,
							width: function() {
								var w = $(window).width();
								if (w < 600) {
									return w - 50;
								} else {
									return 600;

								}
							},
							title: 'About Google Analytics',
							buttons: [{
								text: 'Close',
								click: function() {
									$(this).dialog('close');
								}

							}],
							open: function() {
								$('.ui-dialog').focus();
								$('.ui-resizable-handle').hide();
							}
						});
					})
					.fail(function (jqXHR, textStatus, errorThrown) {
						ga('send', 'event', 'opt out blurb', 'error', errorThrown);
						window.open(optPage);
					});
				//	optOutWin.focus();
				});
				$('#opt-out').on('click', function () {
					_LR.fn.setCookie('lrGAOptOut', 'y', 365, '.' + _LR.domain);
					$(this).html('Opted out').addClass('disabled').after('<em>Your behavior will no longer be tracked by Google Analytics on this site.</em>');
				});


			}());
			// show problem note if applicable
			_LR.fn.showNote('note');



				/*
			// wait for EBSCO to make url change. Also, should only send on first page of results.
			var kw = findSearchTerms();
			if (kw !== '') {
			  ga('send', 'event', 'search log', 'search', kw);
			}
			*/
				// show links to Primo VE beta
				(function () {
					if ((_LR.fn.getCookie('primoPreview') !== 'hide') && (_LR.profile !== 'scccatcomp' )){
						var message = 'Go to the new OneSearch';
						var pages = [
							{
								type: 'basic',
								params: '',
								el: $('.find-field-controls')
						},
							{
								type: 'advanced',
								params: '&mode=advanced',
								el: $('#guided-find-fields')
						},
							{
								type: 'result',
								params: (function () {
									var limits = '&query=any,contains,' + encodeURIComponent(_LR.currentQuery);
									var mes = message.replace('Go to', 'Try this search in');
									if ($('#common_RV').prop('checked')) {
										limits += '&facet=tlevel,include,peer_reviewed';
									} else if ($('#common_FC').prop('checked')) {
										limits += '&tab=books_videos_in_library&search_scope=books_videos_in_library';
									}
									return {
										limits,
										mes
									};
								}()),
								el: $('#header')
						},


					];
						var url = '';
						var buttonAdded = false;
						for (var i = 0; i < pages.length; i++) {
							if (_LR.pageType === pages[i].type) {
								message = pages[i].params.mes || message;
								url = _LR.primoUrlBase + (pages[i].params.limits || pages[i].params);
								pages[i].el.append('<div id="lr-primo-link" class="button icon-btn" style="display:none;"><button type="button" title="Dismiss this message" aria-label="Close">x</button><a href="' + url + '" target="_blank" onclick="ga(\'send\', \'event\', \'Primo VE beta\', \'click\');">' + message + '</a></div>');
								$('#lr-primo-link').fadeIn();
								buttonAdded = true;
							}
						}
						console.log('button added: ' + buttonAdded);
						if (buttonAdded) {
							$('#lr-primo-link button').on('click', function () {
								$('#lr-primo-link').slideUp();
								_LR.fn.setCookie('primoPreview', 'hide', false, _LR.domain);
							});

						}

					}

				}());
				(function() { // hide chat widgets on small screens in certain situations
					var hideWidget = function() {
						$(_LR.currentCol.askWidget).hide();
					};
					if (_LR.smallScreen) {
						$('#footerLinks').prepend('<li><a href="' + _LR.currentCol.homePage + '">' + _LR.currentCol.colName + ' Library</a></li>');
						if (_LR.currentCol.askWidget) {
							if (_LR.pageType === 'basic') {
								var waitWidget = setInterval(function() {
									if ($(_LR.currentCol.askWidget).length) {
										clearInterval(waitWidget);
										hideWidget();
									}
								}, 50);
							}
							$('#innerContainer')
								.on('focusin', 'input', hideWidget)
								.on('focusout', 'input', function() {
									if (/(a|c)rc/.test(_LR.college) === false) { // arc and crc's are not easy to hide/show
										$(_LR.currentCol.askWidget).show();
									}
								});
						}
					}
				}());
				if (_LR.guest() === true)
				{
					// add my EBSCOhost link
					var myEHost = 'https://' + location.hostname + '/eds/Toolbar/SignInOut?sid=' + ep.getUrlParam('sid') + '&vid=' + ep.getUrlParam('vid') + '&ReturnUrl=' + ep.URLEncode(ep.newReturnUrl);
					$('.nav-item').each(function()
					{
						if ($(this).text().indexOf('All Databases') > -1)
						{
							$(this).after('<li class="nav-item" role="menuitem" tabindex="-1"><a class="link color-s1" href="' + myEHost + '">My EBSCOhost</a></li>');
						}
					});
				}
				var eLinks=[ {
					id: 'supportSiteLink',
					page: ''
				},

					{
					id: 'rpLinks_ctl01_link',
					page: 'ehost/privacy.html'
				},
					{
					id: 'rpLinks_ctl02_link',
					page: 'ehost/terms.html'
				},
					{
					id: 'rpLinks_ctl03_link',
					page: 'ehost/terms.html#copyright'
				},
					{
					id: 'rpLinks_ctl04_link',
					page: 'contact/index.php'
				}

				];
				for (var i=0;i < eLinks.length; i++) {
					$( '#ctl00_ctl00__copyrightArea_footer_' + eLinks[i].id).attr( {
						href: '//support.ebscohost.com/' + eLinks[i].page, target: '_blank'
					}
					);
				}

			$('#Toolbar a').on('click', function () {
				var label = $(this).text();
				ga('send', 'event', 'top menu', 'click', label);
			});
			$('#ctl00_ctl00_Column1_Column1_similarArticleControl_FindMoreArticles').on('click', function () {
				ga('send', 'event', 'features', 'click', 'smart search');
			});
			$('#selectDBLink').on('click', function () {
				ga('send', 'event', 'search-box', 'click', 'Change Database');
			});
			$('#history').on('click', function () {
				ga('send', 'event', 'search-box', 'click', 'search history');
			});
			$('#helpLink').on('click', function () {
				ga('send', 'event', 'search-box', 'click', 'search help');
			});
			$('#options').on('click', function () {
				ga('send', 'event', 'search-box', 'click', 'search options');
			});
			$('#AddRemoveFields a').on('click', function () {
				var label = $(this).prop('title');
				ga('send', 'event', 'advanced search', 'click', label);
			});
			$('#footerLinks a').on('click', function () {
				var label = $(this).text();
				ga('send', 'event', 'footer-ebsco', 'click', label);
			});
console.log(_LR);
});
})();