
(function() {
 
 _LR.fn.showTips = function (tipHeadText, tipMessage, tipType) {
  var $ = jQuery;
  var userPref = _LR.fn.getCookie(tipType);
  if (userPref !== 'hide') {
    ga('send', 'event', 'placards', 'display', tipType + ' tip');
    console.log(tipType);
    var noResults = $('#UserMessageLabel');
    var searchMessages = $('.search-messages');
    var searchHints = $('.search-hints');
    // could use the area above the searchbox instead
    // var searchbox = document.querySelector('.searching');
    var resultsList = $('ul.result-list');
    var resultListControl = $('#resultListControl');
    var screenClass = 'search-or-detail'; //max-width: 500px
    if (_LR.pageType === 'result') {
      screenClass = 'results'; // width:90%
    }
    var tipContainer = $('<div />').attr('id', 'tip-container');
    if (tipType === 'course search') {
      tipContainer.attr('id','tip-container-subtle');
    }
    tipContainer.attr({'role': 'alert', 'class': screenClass});
    $('<button />').attr({id: 'close-tip', type: 'button'}).html('x').appendTo(tipContainer).on('click', function() {
      tipContainer.slideUp('slow', function() {
        tipContainer.remove();
        });
      ga('send', 'event', 'placards', 'hide', tipType + ' tip');
      _LR.fn.setCookie(tipType,'hide',false,'.' + _LR.domain);
    });
    tipContainer.append('<h2 class="tip-heading">' + tipHeadText +'</h2><div id="lr-tip-content">' + tipMessage + '</div>');
  
    if (tipType !== 'course search') {
      tipContainer.append('<div id="rating-container"><div><i>Was this tip helpful?</i></div><div id="ratings"> <button type="button" id="rate-yes" >yes</button><button type="button" id="rate-no" >no</button></div></div>');
    }

    // create the div into which the content will be placed
    var warningArea = $('<div />').attr({'id': 'losrios-warning-area', 'aria-live': 'polite'});
    // place div where on the search screens, when there are no results at all
    if (noResults.length) {
      // make sure it's the no results message and not the timeout message. Slight side effect: if that message displays, placards will not.
      var userMessage = noResults.html();
      if (userMessage.indexOf('results') > -1) {
        // if EBSCO's hints display, show ours above those
        if (searchHints.length) {
          searchHints.prepend(warningArea);
        } else {
          searchMessages.append(warningArea);
        }
      }
    } else if (resultsList) {
      resultListControl.prepend(warningArea);
    }
    warningArea.append(tipContainer);
    if ($('#rating-container').length) {
      $('#rate-yes').on('click', function() {
        _LR.fn.feedback('helpful', tipType);
      });
      $('#rate-no').on('click', function() {
        _LR.fn.feedback('unhelpful', tipType);
      });
    }
  }
};
_LR.fn.feedback = function(opinion, tipType) {
  console.log(tipType);
  var $ = jQuery;
  ga('send', 'event', 'feedback - ' + tipType + ' tip', 'rate', opinion);
  // shows after clicking the feedback prompt
  var container = $('#rating-container');

  // cookie to hide in future - need to add functions for this
  if (opinion === 'unhelpful') {
    container.html('<input id="hide-tip" type="checkbox"> <label for="hide-tip"> Stop showing this tip?</label>');
    $('#hide-tip').on('click', function(e) {
      e.preventDefault();
      ga('send', 'event', 'feedback - ' + tipType + ' tip', 'hide');
      $('#losrios-warning-area').fadeOut();
      _LR.fn.setCookie(tipType, 'hide', 60, '.' + _LR.domain);
    });
  } else {

    container.html('Thanks for your feedback!');
    setTimeout(function() {
      container.slideUp();
    }, 3000);
  }
};
 
  // regular expressions
  // phrases such as "scholarly articles" or "articles about x"
  var articles = /(((article(s)?) (on|about|by) .*)|(article(s)?$)|(scholarly|academic) journal(s)?$)/i;
  // call numbers
  var callNo = /(^[a-z]{1,2}\s*\d+(\.\d+)*\s*\.?[a-z]{1,2}\d+.*)/i;
  // questions - could improve. "how to" queries should be a separate one
  // older version. var questions = /((^(what|how|where|when) (do|does|did|is|are|was|were|should|can|could) .*)|why .*|\?)/i;
 // var questions = /(^(what|how|where|when|why) (do|does|did|is|are|was|were|should|can|could) .*)/i;
  // searching for editions
  var editions = /((^|.* )((\d{1,2}|[a-z]{0,10})((st|nd|rd|d|th|\.)| custom| scc| student) (edition|ed\.?))($|\s.*))/i;
  /* var editions = /((\d{1,2}|[a-z]{0,10})((st|nd|rd|d|th|\.)| custom| scc| student) (edition|ed\.?))/i;
   */
  // course codes
  var courseCodes = /^(\d{5})$/;
  // e.g. math 100, in order to show reserves form/link after a search
  // var courseSearch = /[a-z]{2,5}\s*\d{2,3}|"on reserve"/i;
  // citations pasted into the search box
  var citations = /(\. web\.|\. print\.|ebscohost| \(\d{4}\)|\. (\d){4}\. | \d{1,2}\(\d{1,2}\), |available from:|accessed:)/i;
  // pro/con searches
  var proCon = /pro(s)?(\s|\/|$)/i;
  if (articles.test(_LR.currentQuery)) {
    _LR.fn.showTips('Looking for Articles?', '<p>Judging by your keywords, <strong>' + _LR.currentQuery + '</strong>, it looks like you may be looking for articles on a topic. Including the word <strong>article</strong> or <strong>journal</strong> in your search query is not a good strategy.</p><p>Instead, use the important words about your topic, and then use the links on the left to limit to articles from academic journals, magazines and/or news sources.</p>', 'articles');
  } else if (editions.test(_LR.currentQuery)) {
    //   else if (searchTerm.search(editions) > -1) {
    _LR.fn.showTips('Searching for a certain edition?',  '<p>If you use the edition number as a keyword, you often won&#39;t find the book. Instead, search using the title and author and click the item&#39;s title to find the edition.</p><p class="tip-heading">Looking for a textbook?</p>', 'edition search');
  }  else if (courseCodes.test(_LR.currentQuery)) {
    _LR.fn.showTips('Searching for a course textbook?', '<p>You can&#39;t search for textbooks using the 5-digit course code. Instead, use the department and course number and instructor&#39;s last name.</p>', 'course codes');
  } else if (callNo.test(_LR.currentQuery)) {
    // need to replace spaces with plus signs for URL
    _LR.currentQuery = _LR.currentQuery.replace(/\sc\.\d{1,2}$/, '');
    var encodedQuery = _LR.currentQuery.replace(/\s/g, '+');
    _LR.fn.showTips('Searching by Call Number?', '<p>To search by call number, you need to expand your search beyond the normal fields. The simplest way is to add <b>TX</b> before the call number. To do this automatically, click the link below.</p><p><a href="http://search.ebscohost.com/login.aspx?direct=true&authtype=ip,guest&custid=' + _LR.custID + '&groupid=main&profile=' + _LR.profile + '&bquery=TX+&quot;' + encodedQuery + '&quot;&site=eds-live">TX &quot;' + _LR.currentQuery + '&quot;</a></p>', 'call numbers');

  } else if ((_LR.currentQuery.search(proCon) > -1) && (_LR.currentQuery.search(/pro[\-\s](life|choice|abortion|gun|bono|sports)/i) == -1)) {
    console.log('pro con tip');
    _LR.fn.showTips('Looking for a pro-con article?', '<p>It looks like you&#39;re looking for the pros and cons of an issue. Using the terms <strong>pro</strong> and <strong>con</strong> as keywords doesn&#39;t usually work well.</p><p>Instead, try:</p><ul><li>searching for words like <strong>impacts</strong>, <strong>effects</strong>, <strong>drawbacks</strong>, <strong>benefits</strong>, or <strong>controversy</strong>;</li><li>thinking of some specific pros and cons that you already know of, and search for those topics instead of the word pro or con;</li><li>searching for your topic in <a target="_blank" href="http://0-infotrac.galegroup.com.lasiii.losrios.edu/itweb/?db=OVIC">Opposing Viewpoints in Context</a>;<li>finding books and articles that provide an overview of your topic, and look for sections or chapters that discuss pros and cons. A good starting point would be <a href="http://0-infotrac.galegroup.com.lasiii.losrios.edu/itweb?db=GVRL" target="_blank">Gale Virtual Reference Library</a>.</li></ul>', 'pro con');
  } else if (_LR.currentQuery.search(citations) > -1) {
    _LR.fn.showTips('Looking for a certain article?', '<p>It looks like you may have pasted a full citation into the search box. This strategy usually won&#39;t work. Instead, enter the item&#39;s title and author&#39;s name in the search box.</p>', 'citations');
  }
  /*
else if (searchTerm.search(courseSearch) > -1) {


  showTips('Looking for a textbook?', '<button onclick="showReservesWidget();">Textbooks search</button>', 'course search');


}
*/
  else {
    console.log('keywords ok');
  }
})();