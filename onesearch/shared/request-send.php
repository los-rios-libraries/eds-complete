<?php
$http_origin = $_SERVER['HTTP_ORIGIN'];

if (strpos($http_origin, 'losrios.edu') > -1)
{  
    header('Access-Control-Allow-Origin: ' . $http_origin);
}
$user = $_POST['user'];
$item = $_POST['item'];
$dest = $_POST['dest'];
$stat = $_POST['status'];
$message = 'Good News! You have successfully requested ' . $item  . "\r\n\r\n";
$subject = 'Your Library Item Has Been Requested!'; // this is default
if (strpos($stat,'AVAILABLE') > -1) {
    $subject = 'Your Library Item Is On Its Way!';
    $message .= 'More Good News! The item you requested is not currently checked out, so it should be available at ' . $dest . ' in 2 to 3 working days (that means weekends and holidays don\'t count).'. "\r\n\r\n";
}
elseif (preg_match('/DUE|HOLDSHELF|TRANSIT/', $stat)) {
    $message .= 'This might take awhile!  The item you requested is not currently on the shelf, so it will be a little longer before it is available at ' . $dest . '. Because we aren\'t sure when the item will be ready, it could be anywhere from several days to several weeks.' . "\r\n\r\n";
}
$message .= 'We will make sure to send you an email at your official Los Rios email when the item arrives at ' . $dest . '. It will be held there for 5 business days.' . "\r\n\r\n";
$message .= 'If you want to cancel or check the status of your request, head on over to My Library Record at:' . "\r\n" . 'https://lasiii.losrios.edu/patroninfo' . "\r\n\r\n";
$message .= 'Bad News! This email was sent by grumpy internet robots. Please do not reply to this email--no one can respond and it just makes the robots grumpier.' . "\r\n\r\n";
$message .= 'If you have questions, please contact your library for help.' . "\r\n\r\n";
$message .= 'Thank you for using our online request service!' . "\r\n\r\n";
$dashes = '-----------------------------------------';
$message .= $dashes . "\r\n";
$message .= 'The Los Rios Libraries'. "\r\n";
$message .= $dashes;
// $message = wordwrap($message, 70, "\r\n");
$headers = 'From: library@losrios.edu' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
if ($message !== '') { 
$email = mail($user, $subject, $message, $headers);
}
header('Content-Type: text/plain');
if ($email) {
    echo 'success';
    
}
else {
    echo 'fail';
}
?>