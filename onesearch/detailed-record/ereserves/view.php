<?php
$AN = $_GET['an'];
$loisNo = str_replace('lrois.', '', $AN);
$data = file_get_contents('http://lois.losrios.edu/record='.$loisNo);
$data = preg_replace('/\n|\r/s', '', $data);
$page = preg_replace('/^.*?(class="bibLinks">)/', '', $data);
$page = preg_replace('/<div id="e-resource.*$/', '', $page);
$toRemove = array('<Text>', '</text>', '<tr align="center">', '<tr>', '</tr>', '<th>Connect to</th>', '<br />', '</table>');
$page = str_replace($toRemove, '', $page);
$page = preg_replace('/td(\s)?>/', 'li>', $page);
$page = preg_replace('/^/', '<ul>', $page);
$page = preg_replace('/$/', '</ul>', $page);
$page = str_replace('href="/search', 'href="http://lois.losrios.edu/search', $page);
$page = str_replace('href="/article', 'href="http://lois.losrios.edu/article', $page);
include '../../shared/college-vars.php';
?>


<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<title>
Ereserves
</title>
<style type="text/css">
	body {font-family: Arial, Verdana, sans-serif; width:90%; max-width:600px; margin:0 auto;}
	h1 {font-family: Georgia, serif;}
	li {margin-bottom:8px;}
</style>
<?php
include '../../shared/ga.php';
?>
</head>
<body>
	<h1>View E-Reserves Online</h1>
<?php echo $page ?> 

<script>
var links = document.getElementsByTagName('a');
for (var i = 0; i < links.length; i++) {
	links[i].addEventListener('click', function() {
		var linkText = this.textContent;
		ga('send', 'event', 'ereserve items', 'click', linkText);
		});
	}
</script>
</body>
</html>

