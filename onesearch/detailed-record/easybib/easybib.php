<?php
/*
// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
*/
// phpinfo();
ob_start();
$http_origin = $_SERVER['HTTP_ORIGIN'];

 if ((strpos($http_origin, 'losrios.edu')) || (strpos($http_origin, 'ebscohost.com')))
 {  
    header('Access-Control-Allow-Origin: ' . $http_origin);
    header('Access-Control-Allow-Methods: GET, PUT, POST');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
 }
$data =  $_POST['bookData'];
// echo 'data: '.$data;

$bookData = urldecode($data);
// echo $bookData;
// //echo '****************** attempt at array: ' . $bookData['Title'] . '******';
$bookData = json_decode($bookData,true);
$api_key = '';
// key is in file that we don't include in repository
include_once('api_key.php');
 $bookData['key'] = $api_key;
// echo '        bookData: ' .$bookData . ' *************';

$stringToSend = json_encode($bookData);
// echo '          string to send: ' .$stringToSend;
 $stringToSend = urlencode($stringToSend);


// $data_json = json_encode($data_json);
$url = 'https://api.citation-api.com/2.1/rest/cite';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($stringToSend)));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($ch, CURLOPT_POSTFIELDS, $stringToSend);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response  = curl_exec($ch);

curl_close($ch);
 // echo $response;
//var_dump($response);
 $newData = json_decode($response);
// var_dump($newData);
 $bookData = $newData -> data;
 echo $response;
 ob_end_flush();
 exit;
 
?>

