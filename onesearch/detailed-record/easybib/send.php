<?php
ob_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

if ((strpos($http_origin, 'losrios.edu')) || (strpos($http_origin, 'ebscohost.com')))
{  
    header('Access-Control-Allow-Origin: ' . $http_origin);
}
$status = '';
function setVar($key) { // assign post value to variable.
	if (isset($_POST[$key])) {
		return $_POST[$key];
	}
	else {
		return '';
	}
}
$keys = array(
	'subject',
	'url',
	'email',
	'citations',
	'type',
	'title',
	'rtac'
);
// set variables
for ($i = 0; $i < count($keys); $i++) {
	${$keys[$i]} = setVar($keys[$i]);
	
	
}


$citationMarkup = urldecode($citations);
$link = urldecode($url);
if ($subject !== '') {
	
 $subject = preg_replace('/[^A-Za-z0-9\- ]/', '', $subject); // strip out potentially dangerous characters
}

$citationMarkup = str_replace('<li ', '<li style="list-style-type:none; margin-bottom:1em;" ', $citationMarkup);

if (preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email) === false) {
	$status = 'bad email address or header injection';
}
else {
	$headers = 'From: Los Rios Libraries <library@losrios.edu>' . "\r\n";
	$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "Content-Transfer-Encoding:base64 \r\n";
	$style = ' style="font-family:Verdana, Arial, sans-serif; font-size:12px;"';
	$message = "<html><head>";
	$message .= "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
	$message .= '</head><body >';
	if (strpos($type, 'Cite') !== false) {
		$subject = 'Citations from the Library';
		$message .= '<p' . $style . '>You requested the following citations:</p>';
	}
	else {
		if ($subject === '') {
			$subject = 'Book Info from OneSearch';
		}
		$message .= '<p' . $style . '><b>Title: </b>' . $title . '</p>';
		$message .= '<div' . $style . '>' . $rtacTable . '</div>';
	}
	$message .= $citationMarkup;
	$message .= '<p' . $style . '>You may find the record for this item at the following page:</p>';
	$message .= '<p' . $style . '><a href="' . $link . '">' . $link . '</a></p>';
	$message .= '<p' . $style . '><em>Note</em>: This email was sent by grumpy internet robots. Please do not reply to this email&ndash;no one can respond and it just makes the robots grumpier.';
	$message .= '<p' . $style . '>Thanks for using OneSearch, from the Los Rios Libraries!';
	$message .= "</body></html>";
	$messagebody = rtrim(chunk_split(base64_encode($message)));
	
	
	if ($status === '') {
		if ($messagebody !== ''){
			$email = mail($email, $subject, $messagebody, $headers);
		}
		if ($email) {
			$status = 'success';
		}
		else {
			if ($status === '') {
				$status = 'fail';
			}
		}
	}
}
header('Content-Type: text/plain');
// ajax call will read this response to tell user whetehr or not mail was sent
echo $status;

ob_end_flush();
exit;
?>