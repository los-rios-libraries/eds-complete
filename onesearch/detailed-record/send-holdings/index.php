<?php
// the message

$location = $_GET['loc'];
$callNo = $_GET['callno'];
$status = $_GET['status'];
$college = $_GET['college'];
$title = $_GET['title'];
$medium = $_GET['medium'];
switch ($college) {
	case 'arc':
		$collCode = 'amerriv';
		$collURL = 'http://www.arc.losrios.edu/arclibrary.htm';
		break;
	case 'crc':
		$collCode = 'cosum';
		$collURL = 'http://www.crc.losrios.edu/library';
		break;
	case 'flc':
		$collCode = 'ns01057';
		$collURL = 'http://www.flc.losrios.edu/libraries';
		break;
	case 'scc':
		$collCode = 'sacram';
		$collURL = 'http://www.scc.losrios.edu/library';
		break;	
}
$perm = 'http://search.ebscohost.com/login.aspx?authtype=ip,guest&custid='.$collCode .'&groupid=main&direct=true&db=cat01047a&AN='. $_GET['an'] .'&site=eds-live&scope=site';
$ip = $_SERVER['REMOTE_ADDR'];

$textTitle = substr($title, 0, 55);
?>

<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow">
	<title>
		Send Call Number
	</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="validate.js"></script>
	<script>
function showForm(type)
{
  var emailDiv = document.getElementById('email');
  var smsDiv = document.getElementById('sms');
 // var prompt = document.getElementById('prompt');
  var smsLabel = document.querySelector('#send-choice label:first-of-type');
  var emailLabel = document.querySelector('#send-choice label:last-of-type');
  if (type === email)
  {
    emailDiv.setAttribute('class', 'active');
    emailDiv.setAttribute('aria-hidden', 'false');
    smsDiv.setAttribute('class', 'hidden');
    smsDiv.setAttribute('aria-hidden', 'true');
    emailLabel.style.fontWeight = 'bold';
    smsLabel.style.fontWeight = 'normal';
  }
  else if (type === sms)
  {
    emailDiv.setAttribute('class', 'hidden');
    emailDiv.setAttribute('aria-hidden', 'true');
    smsDiv.setAttribute('class', 'active');
    smsDiv.setAttribute('aria-hidden', 'false');
    smsLabel.style.fontWeight = 'bold';
    emailLabel.style.fontWeight = 'normal';
  }
//  prompt.setAttribute('class', 'hidden');
//  prompt.setAttribute('aria-hidden', 'true');
}
</script>
</head>
<body>
<div id="container">
<!--	<h1 id="prompt">How do you want to send the call number?</h1> -->
	<form id="send-choice">
		<input type="radio" checked="checked" id="send-sms" name="send-type" value="sms" onclick="showForm(sms);">
		<label for="send-sms"><img class="icon" src="img/smart-phone.png" alt="">Text message</label>
		<input type="radio" id="send-email" name="send-type" value="email" onclick="showForm(email);">
		<label for="send-email"><img class="icon" src="img/email.png" alt="">Email</label>
				
		</label>
	</form>
<noscript>To submit the form, please disable styles in your browser.</noscript>
<div id="sms" class="active">
	<p>

You are requesting to send the following message to your phone:
</p>
<div class="message">


	<?php echo "<p>" .$textTitle . "</p><p>" . $location . "</p><p>" .$callNo . "</p>" ?>
	
</div>
<p id="data-rates">Normal message and data rates may apply.</p>
<form method="post" id="sendform-sms" class="sendform" action="process.php" onsubmit="return validator(sms);">
	<label for="sms-address">Enter your 10-digit phone number without any spaces, dashes or parentheses</label>
	<input id="sms-address" name="number" type="text">
	<label for="carrier-gateway">Select your wireless carrier</label>
	<select id="carrier-gateway" name="domain">
		<option selected="selected" value="">Select</option>
		<option value="txt.att.net">AT&amp;T</option>
		<option value="myboostmobile.com">Boost Mobile</option>
		<option value="mymetropcs.com">MetroPCS</option>
		<option value="msg.fi.google.com">Project Fi</option>
		<option value="messaging.sprintpcs.com">Sprint</option>
		<option value="tmomail.net">T-Mobile</option>
		<option value="vtext.com">Verizon</option>
		<option value="vmobl.com">Virgin Mobile</option></select>
	<input name="title" type="hidden" value=" <?php echo $textTitle; ?>">
	<input name="location" type="hidden" value="<?php echo $location; ?>">
	<input name="callno" type="hidden" value="<?php echo $callNo; ?>">
	<input name="medium" type="hidden" value="sms">
	<input name="college" type="hidden" value="<?php echo $college; ?>">
	<input type="submit" value="send">
	
</form>
</div>
<div id="email" class="hidden"  aria-hidden="true">
	<p>

You are requesting to email the following message:
</p>
<div class="message">

	<?php echo "<p>" .$title . "</p><p>" . $location . "</p><p>" .$callNo . "</p><p>" . $status . "</p><p class='permalink'>View this record at " .$perm . "</p>" ?>
	
</div>

<form method="post" id="sendform-email"  class="sendform"  action="process.php" onsubmit="return validator(email);">
	<label for="email">Enter your email address</label><input id="email-address" name="address" type="text"><input name="perm" type="hidden" value="<?php echo urlencode($perm) ?>">
	<input name="title" type="hidden" value="<?php echo $title; ?>">
	<input name="location" type="hidden" value="<?php echo $location; ?>">
	<input name="callno" type="hidden" value="<?php echo $callNo; ?>">
	<input name="medium" type="hidden" value="email">
	<input name="college" type="hidden" value="<?php echo $college; ?>">
	<input type="submit" value="send">
	
</form>

</div>
</div>

</body>
</html>