(function()
{
	_LR.cat = 'lois';
	if (ep.clientData.currentRecord.Db === 'cat05942a')
	{
		_LR.cat = 'lrd';
	}
	_LR.lois = {

		showWhat: function()
		{
			var showLess = jQuery('.rtac-table tbody tr:last');
			showLess.hide();
			var hiddenLocs = jQuery('.lr-location:hidden');
			console.log('hiddenlocs length is ' + hiddenLocs.length);
			if (hiddenLocs.length)
			{
				showLess.prev('tr').attr('style', 'display:table-row');
				jQuery('.rtac-show-all').html('More Locations');
			}
		},
		showExtLinks: function()
		{
			var wcBase = 'http://worldcat.org/';
			var zip = '&loc=958' + _LR.currentCol.zipFrag;
			var title = _LR.bookData.Title; // can't use EBSCO token because it may contain quotation marks/apostrophes
			var author = '';
			if (_LR.bookData.Authors)
			{
				author = _LR.bookData.Authors.last;
			}
			else if (_LR.bookData.OtherAuthors)
			{
				var arr = _LR.bookData.OtherAuthors.split(',');
				author = arr[0];
			}
			var authorTitle = encodeURIComponent(author + ' ' + title);
			authorTitle = authorTitle.replace('%2C', '').replace('%3A', '');
			var splLink = '<li><a target="_blank" id="splLink" onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'Sac Public\');" href="http://find.saclibrarycatalog.org/iii/encore/search?formids=target&lang=eng&suite=def&reservedids=lang%2Csuite&submitmode=&submitname=&target=' + authorTitle + '">Search for this item at Sacramento Public Library</a></li>';
			var wcText = 'Search for this item at CSUS, UC Davis and other libraries';
			var oclcLink = '';
			var oclcGA = 'onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'Worldcat\');"';
			if (_LR.itemData.oclc !== '')
			{
				oclcLink = '<li><a id="oclcLink" ' + oclcGA + ' target="_blank" href="' + wcBase + 'oclc/' + _LR.itemData.oclc() + zip + '">' + wcText + '</a></li>';
			}
			else if (_LR.itemData.isbn !== '')
			{
				oclcLink = '<li><a id="oclcLink" ' + oclcGA + ' target="_blank" href="' + wcBase + 'isbn/' + _LR.itemData.isbn() + zip + '">' + wcText + '</a></li>';
			}
			var secondaryRequestMarkup = '<ul id="secondary-request">' + splLink + oclcLink + '</ul>';
			var secondaryLinks = document.getElementById('secondaryLinks');
			if (!(document.getElementById('secondaryLinksList')))
			{
				var secondaryLinksList = document.createElement('div');
				secondaryLinksList.id = 'secondaryLinksList';
				secondaryLinksList.innerHTML = secondaryRequestMarkup;
				secondaryLinks.appendChild(secondaryLinksList);
				var secondaryLinksButton = document.getElementById('secondaryLinksToggle');
				secondaryLinksButton.setAttribute('aria-pressed', 'true');
				secondaryLinksButton.textContent = '- Search other libraries';
				//    secondaryLinksButton.setAttribute('onclick', pathToFrame + '._LR.lois.hideExtLinks()');
				secondaryLinksButton.removeEventListener('click', _LR.lois.showExtLinks);
				secondaryLinksButton.addEventListener('click', function(e)
				{
					e.preventDefault();
					_LR.lois.hideExtLinks();
				});

			}


		},
		hideExtLinks: function()
		{
			var secondaryLinks = document.getElementById('secondaryLinks');
			secondaryLinks.removeChild(secondaryLinks.childNodes[1]);
			var secondaryLinksButton = document.getElementById('secondaryLinksToggle');
			secondaryLinksButton.textContent = '+ Search other libraries';
			secondaryLinksButton.setAttribute('aria-pressed', 'false');
			secondaryLinksButton.removeEventListener('click', _LR.lois.hideExtLinks);
			secondaryLinksButton.addEventListener('click', function(e)
			{
				e.preventDefault();
				_LR.lois.showExtLinks();
			});

		},
		openRequest: function(num, type)
		{
			var $ = jQuery;
			// place iframe inside ebsco  page
			var holdingsArea = $('.record-has-rtac');
			var requestFrameContainer = $('#lr-frame-container');
			requestURL = '';
			var height = '';
			var frameClass = '';
			var frameOffset = '';
			var frameParent = holdingsArea;
			if (type == 'request')
			{
				requestURL = 'https://lasiii.losrios.edu/search~S9?/.' + num + '/.' + num + '/1,1,1,B/request~' + num;
				height = '640';
				frameClass = 'lr-req';
				frameOffset = '-90';
				if (_LR.smallScreen)
				{ // in april 2018 EBSCO launched responsive site--this is important for small screens
					window.open(requestURL);
					return false;
				}
			}
			else if (type == 'ereserve')
			{
				requestURL = '//' + _LR.servers.domain + _LR.servers.root + '/onesearch/detailed-record/ereserves/view.php?college=' + _LR.college + '&an=' + num;
				height = '500';
				frameClass = 'lr-eres';
				frameOffset = '0';
				frameParent = $('.citation-wrapping-div');
			}
			var closerX = '<button type="button" id="reqframe-closer" aria-label="close"> X </button>';
			if (type === 'ereserve')
			{
				closerX = '';
			}
			var frameMarkup = '<div class="lr-frame-closer"><a id="req-newwin" href="' + requestURL + '" target="blank" onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'' + type + ' new window\');">Open this in a new window</a>' + closerX + '</div><div id="request-container"><div id="request-scroll"><iframe style="display:none;" class="' + frameClass + '" id="request-frame" width="100%" height="' + height + '" src="' + requestURL + '"></div></div></iframe>';
			// remove request button
			$('#lr-conditional-request').fadeOut();
			if (!(requestFrameContainer.length))
			{
				requestFrameContainer = $('<div>').attr(
				{
					'id': 'lr-frame-container',
					'style': 'display:none;',
					'tabindex': '0'
				}).html(frameMarkup).appendTo(frameParent);
				$('#request-frame').on('load', function()
				{
					$(this).show(1, function()
					{
						requestFrameContainer.slideDown();
					});
				});

			}
			else
			{
				requestFrameContainer.html(frameMarkup);
			}
			setTimeout(function()
			{
				$('#reqframe-closer').on('click', function(e)
				{
					_LR.lois.closeReq();
					e.preventDefault();

				});

			}, 100);
			$('#lr-frame-container').focus();
		},
		closeReq: function()
		{
			var $ = jQuery;
			var a = $('#lr-frame-container');
			a.slideUp('slow', function()
			{
				a.remove();
				$('#lr-conditional-request').fadeIn();
			});
		},
		parseAuthor: function(str)
		{
			console.log(str);
			// get first, middle and last name
			var first, middle, last, fun;
			var arr = str.split(',');
			var author = false;
			var editor = false;
			var translator = false;
			if (arr.length > 1)
			{ // avoid corporate authors, which won't generally have a comma
				//code
				var lastArrEll = arr[arr.length - 1];
				if (lastArrEll.indexOf('author') > -1)
				{ // last member of array
					arr.pop(); // remove author designation
					author = true; // flag as  an author
				}
				else if (lastArrEll.indexOf('editor') > -1)
				{ // last memeber of array
					arr.pop(); // remove editor designation
					editor = true; // flag as editor
				}
				else if (lastArrEll.indexOf('translator') > -1)
				{ // last memeber of array
					arr.pop(); // remove translator designation
					translator = true; // flag as translator
				}
				else if (/illustrator|photogra|host institu|contributo|introducti|(after|fore)word/.test(lastArrEll)) { // may need to add more non-author relator terms here
					return ''; // don't include these in the json string sent to easybib at all--only confuses
				}
				console.log(arr);
				for (var i = 0; i < arr.length; i++)
				{
					arr[i] = arr[i].trim();
					if (/\d{4}/.test(arr[i]))
					{ // year, e.g. for lifespan
						arr.splice(i, 1); // remove year statement
					}
				}
				if (arr.length > 1) { // otherwise this is a corporate author... although could be problematic for one-name authors e.g Cher
					last = arr[0]; // first member of array should be last name
					// console.log(arr[0]);
					arr.shift(); // remove it from teh array
				
				

					var firstArr = arr[0].split(' '); // separate remaining part
					first = firstArr[0]; // of this, first member should be first name
					firstArr.shift(); // remove first name
					middle = firstArr.join(' '); // anything remaning should be middle name
					console.log(middle);
					middle = middle.replace(/\(.*/, '').trim(); // authorized headings often include parentheses here../
				}
				else {
					return '';
				}
			}
			else
			{
				return ''; // corporate authors not really used in book citations are they? could instead return arr[0];
			}
			if (author === true)
			{
				fun = 'author';
			}
			else if (editor === true)
			{
				fun = 'editor';
			}
			else if (translator === true)
			{
				fun = 'translator';
			}
			else if (!(_LR.bookData.Authors))
			{
				fun = 'editor'; // if nobody is listed under the Authors dt, then it is definitely an edited volume
			}
			else
			{
				fun = 'author'; // this is questionable but not clear how else to do it
			}
			//console.log(_LR.bookData[key]);
			return { // these are teh object properties usedd by Easybib
				'function': fun,
				'last': last,
				'first': first,
				'middle': middle
			};

		},
		bookDisplayCtrl: function(widgetType)
		{
			var $ = jQuery;
			var books = $('#' + widgetType + ' + div .item');
			books.each(function()
			{
				var a = $(this);
				var aHTML = a.html();
				var badDBs = /Harvard Library|HathiTrust/; // identifying these in case we want to style them somehow
				var LOIS = 'LOIS: Los Rios Online'; // local catalog books
				if (badDBs.test(aHTML) === true)
				{
					a.addClass('not-in-LOIS');
				}
				else if (aHTML.indexOf(LOIS) > -1)
				{
					var libRibbon = $('<div />').attr('class', 'lib-ribbon').html('In the Library');
					if (aHTML.indexOf('In the Library') === -1)
					{
						a.prepend(libRibbon);
						a.addClass('in-LOIS');
					}
				}
			});
		}
	};

}());
if (_LR.cat === 'lois')
{
	jQuery('.custom-link-item:last-of-type, .custom-link-item:nth-last-of-type(2)').hide(); // these are not full-text links so they appear at the end

}



(function($)
{
	_LR.bookData = {}; // this will be object to use for easybib and perhaps other things...
	// build _LR.bookData object from citation fields
	var a = $('#citationFields dt');
	a.each(function()
	{
		var key = $(this).text().trim();
		var value = $(this).next().text();
		key = key.replace(/:/, '').replace(/ /g, ''); // remove colon from end and also remove spaces to make these work as keys
		_LR.bookData[key] = value; // build initial object
		// when there are multiple values in e.g. additional authors/editors, the dts are blank; join them in single dd separated by semicolons. Can use that to build arrays later
		if (/\w/.test(key) === false)
		{
			var prevDTs = $(this).prevAll('dt'); // creates array-like object ordered from nearest to furthest, so can find first matching one and then break loop
			// do a loop and search for next one that has text
			prevDTs.each(function()
			{
				if (/\w/.test($(this).text()) === true)
				{ // find nearest dt with a word in it
					var editDT = $(this).text().replace(' ', '').replace(/:$/, ''); // still need to remove colon and spaces, because grabbing key from dt again 
					_LR.bookData[editDT] += ';' + value; // add value from orphaned dd to existing dd after semicolon
					return false; // break the loop
				}
				else
				{
					return true;
				}
			});
			//   console.log(headDT);
			// console.log(headDT.next().text().replace(/$/, ';' + $(this).next().text()));

			//    _LR.bookData[headDT] = value + ';' +  $(this).next().text();
		}
	});
	_LR.bookData.Title = $('.citation-title').text().replace(/ :/g, ':').replace(/\[.*\]/, '').replace(/ \/ .*/, ''); // title in citation fields includes statement of responsibility. Override it with this value
	// get city and publisher from single line of publication info
	if (_LR.bookData.PublicationInformation)
	{
		var pubInfo = _LR.bookData.PublicationInformation.replace(/\[|\]/g, '');

		var arr = pubInfo.split(':'); // colon follows city
		var city = arr[0].trim();
		_LR.bookData.City = city; // add it to the object
		arr.shift(); // remove city from the array
		var newStr = arr.join(' ');
		var newArr = newStr.split(','); // comma precedes year
		newArr.pop(); // remove year
		var final = newArr.join(' ');
		// perform substitutions
		_LR.bookData.Publisher = final.trim(); // add the remainder as publisher. Might still need to work on removing extra stuff that is sometimes there
	}

	if (_LR.bookData.Authors)
	{
		// break down authors into first, middle and last names
		_LR.bookData.Authors = _LR.lois.parseAuthor(_LR.bookData.Authors);
	}

(function()
{ // include links for series
	var dt = $('#citationFields dt:contains("Series")');
	if (dt.length)
	{
		var linkMarkup = '';
		var dd = dt.next(); // the dd
		var arr = dd.html().split('<br>'); // multiple series may be listed and are split with br
		for (var i = 0; i < arr.length; i++)
		{
			var el = $('<div />').html(arr[i]); // need to put these in containing div in order to get the text properly
			var arr2 = el.text().split(';'); // when record includes number, semicolon separates it from series name; need not to include it in the link
			linkMarkup += '<a href="javascript:__doLinkPostBack(\'\',\'ss~~%22' + arr2[0] + '%22%7C%7Csl~~rl\',\'\');">' + el.html() + '</a>'; // html is used in order to preserve bolded stuff
			if (i !== (arr.length - 1))
			{
				linkMarkup += '<br>'; // reintroduce br element unless it is the last item
			}
		}
		dd.html(linkMarkup);
	}

}());
})(jQuery);


if (_LR.cat === 'lois')
{
	(function()
	{


		// place request link under rtac table. Need to delay execution so that holdings load first.
		var rtacCall = setInterval(function()
		{
			if (document.querySelector('.rtac-table') === null)
			{
				console.log('rtac nto found');
			}
			else
			{
				console.log('rtac found');
				clearInterval(rtacCall);
				// first get text from table



				var requestLinkSpace = document.createElement('div');
				requestLinkSpace.id = 'lr-conditional-request';
				var rtacLocs = document.querySelectorAll('.rtac-table tbody td:first-child');
				var loisNo = _LR.itemData.an().replace('lrois.', '');
				var requestURL = 'https://lasiii.losrios.edu/search~S9?/.' + loisNo + '/.' + loisNo + '/1,1,1,B/request~' + loisNo;
				var secondaryLinksBut = '<div id="secondaryLinks"><button id="secondaryLinksToggle" type="button" onclick="ga(\'send\', \'event\', \'catalog record\', \'toggle\', \'secondary links\');">+ Search other libraries</button></div>';
				var rtacArea = document.querySelector('.record-has-rtac');
				var ftLink = $('.custom-link-item:contains(Full Text)');
				if (ftLink.length) {
					if (rtacArea.innerHTML.indexOf('ONLINE ACCESS') === -1) {
						ftLink.hide();
					}
				}

				// adjust locations
				var collegeABR = _LR.college.toUpperCase();
				var rtacRows = jQuery('.rtac-table tbody tr');
				rtacRows.addClass('lr-location');
				var showLess = jQuery('.rtac-table tbody tr:last');
				var showMore = showLess.prev('tr');
				showLess.removeClass('lr-location');
				showMore.removeClass('lr-location');
				//     console.log('total number of rows is ' +rtacRows.length);
				var localItems = jQuery('.rtac-table tr:contains(' + collegeABR + ')');
				//     console.log('local items length of ' + collegeABR + ' is ' + localItems.length);
				if (rtacRows.length > 7)
				{ // only change behavior if there are more than 5 items - 2 extra rows for show more/less
					if (localItems.length)
					{
						if (localItems.length > 5)
						{ // if there are more than five local items, show only the first four by default
							rtacRows.removeClass('sel-rtac-first');
							localItems.slice(0, 4).attr('class', 'sel-rtac-first').show();
							jQuery('.lr-location').hide();
						}
						else
						{ // if there are 5 or fewer, show them all.
							rtacRows.removeClass('sel-rtac-first');
							localItems.attr('class', 'sel-rtac-first').show();
							jQuery('.lr-location').hide();
						}
						_LR.lois.showWhat();
					}

					else
					{ // note: if there is only one item, the show more and show less rows are not there, which means we don't want to call the _LR.lois.showWhat function.

						if ((rtacRows.length > 1) && (rtacRows.length < 8))
						{
							_LR.lois.showWhat();
						}
						else if (rtacRows.length === 1)
						{

						}
						else
						{
							rtacRows.removeClass('sel-rtac-first');
							rtacRows.slice(0, 4).attr('class', 'sel-rtac-first').show();
							$('.lr-location').hide();
							_LR.lois.showWhat();
						}
					}
				}




			}

		}, 200);


	})();




	(function($)
	{
		// highlight reserves notes
		$('dt').each(function()
		{
			var a = $(this);

			if (a.text().indexOf('Notes:') === 0)
			{

				a.attr('id', 'opac-notes');
			}
		});
		var noteDD = $('#opac-notes + dd');
		if (noteDD.length)
		{
			if (noteDD.text().indexOf('ON RESERVE AT') > -1)
			{
				noteDD.attr('id', 'reserves-note');
				noteDD.html(noteDD.html().replace(/LOCAL/g, ''));
			}
			// remove isbns from notes fields
			noteDD.html(noteDD.html().replace(/\d{10}.*|\d{13}.*|\d{9}X.*/g, ''));
			if (noteDD.text() === '')
			{
				noteDD.hide();
				noteDD.prev().hide();
			}

		}
	}(jQuery));



	(function($)
	{
		similarString = 'related_information_widget_similar_books';
		otherByString = 'related_information_widget_other_books_by_this_author';
		$('#' + similarString + ' + div h2').on('click', function()
		{

			_LR.lois.bookDisplayCtrl(similarString);
		});

		$('#' + otherByString + ' + div h2').on('click', function()
		{

			_LR.lois.bookDisplayCtrl(otherByString);
		});

	}(jQuery));


}
(function($)
{
	// show LOIS link
	var arr = ep.clientData.currentRecord.Term.split('.');
	var primoVEUrl = _LR.primoRoot +'fulldisplay?docid=alma' + arr[1] + '&amp;' + _LR.primoView;
	var viewInPrimo = '<span class="primo-link"><a onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'Primo\');" href="' + primoVEUrl + '" target="_blank">View in OneSearch</a></span>';
	var dbField = $('#citationFields dd:last-of-type');
	dbField.html(dbField.html() + '. ' + viewInPrimo);

}(jQuery));

(function($)
{
	// add images for Kanopy
	_LR.fn.hideBox = function()
	{
		$('.citation-image-box').remove();
	};
	if (!($('#bookJacket').length))
	{
		var link = $('#linkOnlineAccess');
		if (link.length)
		{
			var url = link.attr('href');
			if (url.indexOf('kanopy') > -1)
			{
				url = url.replace(/\/$/, ''); // remove trailing slash if it's there
				var arr = url.split('/');
				var no = arr.pop();
				var filmImg = 'https://www.kanopy.com/node/' + no + '/external-image';
				var kanString = '<dt class="hidden-access">Book Image: </dt><dd class="citation-image-box"><div id="bookJacket"><img src="' + filmImg + '" alt="Video image" onerror="_LR.fn.hideBox();"> </div></dd>';
				if ($('#lr-permalink-dd').length)
				{ // since we added this feature, image needs to go after it - should load before this script does
					$('#lr-permalink-dd').after(kanString);
				}
				else
				{
					$('.citation-title').after(kanString);
				}
			}
		}

	}

}(jQuery));
_LR.easyBib = {
	errorLogged: 'This error has been logged and we will follow up on it.',
	sortList: function(selector)
	{ // reference: https://www.w3schools.com/howto/howto_js_sort_list.asp
		var i, b, shouldSwitch;
		var list = document.getElementById(selector);
		var switching = true;
		/* Make a loop that will continue until
		no switching has been done: */
		while (switching)
		{
			// Start by saying: no switching is done:
			switching = false;
			b = list.getElementsByTagName('li');
			// Loop through all list items:
			for (i = 0; i < (b.length - 1); i++)
			{
				// Start by saying there should be no switching:
				shouldSwitch = false;
				/* Check if the next item should
				switch place with the current item: */
				if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase())
				{
					/* If next item is alphabetically lower than current item,
					mark as a switch and break the loop: */
					shouldSwitch = true;
					break;
				}
			}
			if (shouldSwitch)
			{
				/* If a switch has been marked, make the switch
				and mark the switch as done: */
				b[i].parentNode.insertBefore(b[i + 1], b[i]);
				switching = true;
			}
		}
	},
	makeEasyBibJSON: function()
	{
		// creates object with properties to be used by Easybib API. Starts with _LR.bookData object built from #citationFields. reference: http://developer.easybib.com/citation-formatting-api/citation-formatting-api-api-spec-v2-1/
		// should probably move this to PHP script - could send needed bookData properties (or entire object) and process it server-side.

		window.easyBibObj = {
			//key: '', // will be added server-side
			book:
			{},
			pubtype:
			{
				main: 'pubnonperiodical'
			},
			pubnonperiodical:
			{
				title: _LR.bookData.Title.replace(/ :/g, ':').replace(/ ;/g, ';').replace(/ \= .*/, '') //  normalize punctuation. last replacement gets rid of parallel titles
			},
			contributors: [],
			source: 'book'

		};


		//easyBibObj.pubnonperiodical.title = _LR.bookData.Title.replace(/ :/g, ':').replace(/ ;/g, ';');
		if (_LR.bookData.Publisher)
		{
			easyBibObj.pubnonperiodical.publisher = _LR.bookData.Publisher.replace(/ ?;.*|Pub\.|in assoc.*|((an?) ?(member|imprint|division) of.*)|\d{4}/ig, '').trim();
		}
		if (_LR.bookData.City)
		{
			easyBibObj.pubnonperiodical.city = _LR.bookData.City.replace(/ ?;.*/, ''); // replace multiple cities, just use first
		}
		if (_LR.bookData.PublicationDate)
		{
			easyBibObj.pubnonperiodical.year = _LR.bookData.PublicationDate;
		}
		if (_LR.bookData.Edition)
		{
			if ((/\d|th|st|nd|rev/i.test(_LR.bookData.Edition)) && (/first|1st/i.test(_LR.bookData.Edition) === false))
			{ // avoid sending first edition statements


				easyBibObj.pubnonperiodical.edition = _LR.bookData.Edition.replace(/ ed.*/i, ''); // just send the number
			}
		}
		if (_LR.bookData.Authors)
		{ // EDS only puts one author in the main author field. This has already been parsed during creation of bookData object.
			easyBibObj.contributors[0] = {};
			easyBibObj.contributors[0]['function'] = 'author';
			easyBibObj.contributors[0].first = _LR.bookData.Authors.first;
			easyBibObj.contributors[0].middle = _LR.bookData.Authors.middle;
			easyBibObj.contributors[0].last = _LR.bookData.Authors.last;
		}
		if (_LR.bookData.OtherAuthors)
		{
			var arr = _LR.bookData.OtherAuthors.split(';'); // they were joined previously when building bookData object
			//    console.log(arr);
			for (var i = 0; i < arr.length; i++)
			{
				if (arr[i].indexOf(',') > -1)
				{ // avoid corporate names
					easyBibObj.contributors.push(_LR.lois.parseAuthor(arr[i]));
				}
			}
		}


		//console.log(easyBibObj);
		// jsonObj = encodeURIComponent(JSON.stringify(easyBibObj));

		//  return jsonObj;
		easyBibObj.formats = [ // add as many or few as we want to show. Certain operations depend on length of this array, but they calculate it dynamically
			{
				label: 'APA 6<sup>th</sup> edition',
				style: 'apa'
			},
			{
				label: 'Chicago/Turabian',
				style: 'chicagob'
			},
			{
				label: 'MLA 8<sup>th</sup> edition',
				style: 'mla8'
			}


		]; // build array to be sent to function that posts to the php script
		// objects that go in the array. need to include style value and label used for DT in EDS
		return easyBibObj;



	},
	runEasyBib: function(cb)
	{


		// get json object from needed values of _LR.bookData object
		var obj = _LR.easyBib.makeEasyBibJSON();

		// create element to append generated citations to
		if (!($('#easybib-ul').length))
		{ // if we've already retrieved it, upon close it is stored on the page, so don't need to query again. Consider using local storage or perhaps server cache to require fewer API calls.

			jQuery('#content').append('<ul id="easybib-ul" style="display:none;"></ul>'); // create hidden div on page to store this

			var arr = easyBibObj.formats;

			for (var i = 0; i < arr.length; i++)
			{ // post each to the script
				// add style to json
				obj.style = arr[i].style;
				// process object for posting
				var jsonString = encodeURIComponent(JSON.stringify(obj));
				// where we are posting to
				var postURL = _LR.servers.domain + _LR.servers.root + '/onesearch/detailed-record/easybib/easybib.php';
				_LR.easyBib.sendEasyBib(jsonString, postURL, arr[i].label); // include label as parameter
			}
		}


		cb();


	},
	sendEasyBib: function(jsonString, url, label)
	{
		//console.log('string being sent:');
		//console.log(jsonString);
		var $ = jQuery;
		var errorMessage = 'Sorry, the citation could not be retrieved at this time. ' + _LR.easyBib.errorLogged;
		var labelToClass = label.slice(0, 3).toLowerCase(); // used to provide class name for element
		$.post(url, 'bookData=' + jsonString) // include name of object for use by php
			.done(function(data)
			{
						//console.log(data);
				//console.log('response received');
				var a = JSON.parse(data);
				var str = '<li class="' + labelToClass + '"><div class="lr-cite-label">' + label + '</div>';
				if (a.status === 'ok')
				{
					str += '<div class="lr-cite-value">' + a.data;
					// console.log(str);
				}
				else
				{
					// if api call fails, should get a message - can send it to GA
					var checkMessage = function()
					{
						if (a.message)
						{
							return message;
						}
						else
						{
							return '';
						}
					};
					str += '<div class="lr-cite-value error">' + errorMessage; // this will look ugly...
					ga('send', 'event', 'custom citations - retrieve', 'error', checkMessage());
				}
				str += '</div></li>';
				$('#easybib-ul').append(str);

			})
			.fail(function(a, b, c)
			{
				console.log('easybib call error: ' + a + '; ' + b + '; ' + c);
				// figure out something to do if ajax fails
				ga('send', 'event', 'custom citations - retrieve', 'error', c);
				$('#easybib-ul').append(errorMessage);
			});
	},
	fixEditions: function()
	{
		// unfortnately EasyBib is not including edition statements in response for apa and chicago, so ehre is workaround.
		var ed = easyBibObj.pubnonperiodical.edition;
		if (ed)
		{
			var substitutions = [
			{
				id: 'apa',
				str: ' (' + ed + ' ed.)'

			},
			{
				id: 'chi',
				str: ', ' + ed + ' ed'
			}];
			for (var i = 0; i < substitutions.length; i++)
			{

				$('#easybib-ul li.' + substitutions[i].id + '').html($('#easybib-ul li.' + substitutions[i].id).html().replace('</i>', '</i>' + substitutions[i].str));

			}

		}
	}




};
(function($)
	{ // easybib statements
		if (typeof(_LR.bookData) === 'object')
		{
			_LR.listSorted = false;

			var changeTool = function(selector, heading)
			{ // hide EBSCO's default tool and show our replacement
				var clone = $(selector).parent().prop('outerHTML');
				$(selector).parent().after(clone);
				$(selector + ':first').parent().hide();
				// need to remove the data-panel to cancel the default click event
				$(selector + ':eq(1)').removeAttr('data-panel').addClass('lr-easybib').html(heading);

			};
			if (_LR.bookData.PublicationType.indexOf('Book') === 0)
			{ // only using this for books right now; can add videos and maybe ebooks later
				changeTool('.cite-link', 'Cite');
				changeTool('.email-link', 'Email');

				$('.lr-easybib').on('click', function(e)
				{
					e.preventDefault();
					var head = $(this).text();
					if (_LR.smallScreen)
					{ // this is necessary in mobile view, otherwise panel stays open
						$('#column2 a.collapsible-toggle').click();
					}
					// runEasyBib retrieves the citations
					_LR.easyBib.runEasyBib(function()
					{
						var container = $('#ToolPanelContent');
						if (!($('#citeDialog').length))
						{
							container.find('.wrapper').html('');
							// many classes below copied from standard EBSCO markup. Classes without presentational value are removed. Less than ideal to be including this much markup in JS but not sure of alternative. could put it in a php file and grab via AJAX but is that better? perhaps use ajax and then save to localStorage.
							var area = '<div id="citeDialog" class="border"><div id="easyBibDialog"><div><h2 class="panel-header color-p2" tabindex="0"><span class="icon cite-link"></span>' + head + '</h2><div class="panel-content"><p class="informational-note"><span class="icon"></span><em><strong>NOTE:</strong></em> The following citations were generated automatically and may contain errors. <strong>Pay special attention to personal names, capitalization, and dates.</strong> Always consult your library resources for the exact formatting and punctuation guidelines. </p></div><div class="panel-footer"><h3 class="lr-email-cite">Email citations</h3><form id="emailCiteForm"><label for="emailCite">E-mail to: </label><input id="emailCite" value="" type="text"><div class="subject" style="display:none;"><label for="emailCiteSubj">Subject: </label><input id="emailCiteSubj" value="" type="text"></div><div><input id="sendCite" class="button primary-action" value="Send" type="submit"><input class="button cancel" value="Cancel" type="button"></div></form></div></div></div>';
							container.find('.wrapper').append(area);
							container.show();
							_LR.fn.showLoader($('.panel-content'));
							if (head === 'Email')
							{ // adjustments to display for email tool
								$('#emailCiteForm').prependTo($('.panel-content')).find('.subject').show();
								$('#citeDialog h3').html('Email book info').insertBefore($('.panel-content'));
							}

							$('#easybib-ul').appendTo($('.panel-content'));
							_LR.fn.loadSubj('#emailCiteSubj');

							if (_LR.listSorted === false)
							{
								var waitToSort = setInterval(function()
								{ // each item is pulled in from separate API call asyncrhonously. we want to wait until they are all there before ssorting them.
									if ($('#easybib-ul li').length === easyBibObj.formats.length)
									{
										clearInterval(waitToSort);
										_LR.easyBib.sortList('easybib-ul');
										_LR.listSorted = true;
										/* fix edition statements for APA and Chicago */
										_LR.easyBib.fixEditions();
										_LR.fn.removeLoader();
										$('#easybib-ul').show();

									}

								}, 50);

							}
							else
							{
								$('#easybib-ul').show();
								_LR.fn.removeLoader();
							}



							container.find('.close-panel, .cancel').on('click', function()
							{
								container.hide();
								$('#easybib-ul').hide().appendTo($('body'));
								$('#citeDialog').remove();

							});
							$('#emailCiteForm').on('submit', function(e)
							{
								e.preventDefault();
								$(this).find('.lr-error').remove();
								// validate email address
								var email = $('#emailCite');
								if (
									/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.val()) === false)
								{
									email.after('<div class="error lr-error" role="alert">Please provide a valid email address</div>');
									email.select();
								}
								else
								{
									var sendError = '<p>There was a problem emailing the citations. Sorry! Please copy and paste them instead.</p>';
									var showMsg = function(msg)
									{
										$('#emailCiteForm').fadeOut('fast', function()
										{
											if (head.indexOf('Cite') > -1)
											{ // placement of error depends upon where send/cancel buttons were.
												$('#citeDialog .panel-footer').html(msg);
											}
											else
											{
												$('#easyBibDialog').html(msg);
											}
										});


									};
									// for sending to email script, need to remove some html elements and add inline styles
									var clone = $('#citeDialog .panel-content').clone();
									var emailFonts = {
										fontFamily: 'Verdana, Arial, sans-serif',
										fontSize: '12px'
									};
									clone.find('*').css(emailFonts);
									clone.find('.lr-cite-value').css(
									{
										marginLeft: '2em',
										textIndent: '-2em'
									});
									clone.find('.lr-cite-label').css('font-weight', 'bold');
									clone.find('form').remove();
									var rtacClone = '';
									if ($('.rtac').length)
									{
										rtacClone = $('.rtac').clone();
										rtacClone.find('*').css(emailFonts);
										rtacClone.find('table').css('border', '1px solid #ccc');
										rtacClone.find('th, caption').css('text-align', 'left');
										rtacClone.find('.text-this, .td-toggle').remove();
									}


									// send form
									$.post(_LR.servers.domain + _LR.servers.root + '/onesearch/detailed-record/easybib/send.php',
										{
											email: $('#emailCite').val(),
											subject: $('#emailCiteSubj').val(),
											citations: encodeURIComponent(clone.html()),
											url: encodeURIComponent(_LR.permalink),
											title: _LR.bookData.Title,
											college: _LR.currentCol.colName, // not currently  using this but could, for college-level specificity
											type: head,
											rtac: encodeURIComponent(rtacClone.html())
										})
										.done(function(response)
										{
											console.log(response);


											if (response === 'success')
											{

												showMsg('<p role="alert">Your email has been sent.</p>');
												ga('send', 'event', 'custom citations - email', 'send');
											}
											else
											{
												showMsg(sendError);
												ga('send', 'event', 'custom citations - email', 'error', response);
												//console.log(response);
											}

										})
										.fail(function(a, b, c)
										{
											showMsg(sendError);
											ga('send', 'event', 'custom citations - email', 'error', c);
										});
								}


							});

						}

						$('.panel-header').focus(); // this is important; brings browser focus to top of panel.

					});
				});
			}

		}
	
	
	}
	
	
	
	(jQuery));