jQuery(function ($)
{
  _LR.fn.adjRTACRes = function(el) {
  	var count = 0;
  	var tables = 0;
  	var textofrow = '';
  	var college = _LR.college.toUpperCase();
  	count = 0;
  	tables += 1;

  	el.find('tbody tr').each(function() {
  		textofrow = $(this).text();
  		if (!(textofrow.indexOf("Show More") > 0)) {
  			$(this).css("display", "none");
  			$(this).attr("class", "");
  		}
  	});



  	el.find('tbody tr:contains("' + college + '")').each(function() {
  		$(this).css("display", "");
  		$(this).attr("class", "sel-rtac-first");
  		count += 1;
  	});

  	if (count === 0) {
  		el.find('tbody tr:first').each(function() {
  			$(this).css("display", "");
  			$(this).attr("class", "sel-rtac-first");
  		});
  	}
  	var showMore = el.find('.rtac-show-all');
  	var showMoreNumber = el.find('.rtac-show-all').text().match(/\d{1,2}/);
  	var thisLocation = el.find('.sel-rtac-first');
  	if (Number(showMoreNumber) === (thisLocation.length - 1)) {
  		showMore.hide();
  	} else {
  		showMore.text('More Locations');
  		showMore.closest('tr').show();
  	}


  };
  
  _LR.fn.condReqRes = function(el) {
  
        var table = el.find('tbody').text();
        var requestLinks = el.find('.custom-link a[title~="request"]');
        requestLinks.hide();
          var rtac = el.find('.rtac');
          if (rtac.length)
          {
            var record = rtac.data('rtac').RecordAN;
            if (record.indexOf('lrois') > -1)
            {
              if (_LR.circulating.test(table) === true)
              {
                
				if (!(_LR.smallScreen)) {
					requestLinks.removeAttr('onclick'); // this needs to be removed, otherwise teh regular custom link behavior takes place. Not clear if this will effect stats in EBSCOadmin--seems likely
					requestLinks.on('click', function(e) {
						e.preventDefault();
						_LR.fn.showReqRes(record, el);
						$('.results-lois-request-area').focus();
					});
					
				}

              requestLinks.appendTo(el).addClass('button lois-req-but').fadeIn().css({'margin-left': '90px','display': 'inline-block'});
              }
            var custLinks = el.find('.custom-link');
            if (custLinks.length === 1) {
              custLinks.css('height', '0');
            }
          }
        }
  
};
 _LR.fn.showReqRes = function(record, item) {
	var loader = '<img src="' + _LR.servers.domain + _LR.servers.root + '/onesearch/reserves-search/res/loader.gif" id="iframe-loading" alt="loading"> ';
	item.append(loader);
 	record = record.replace('lrois_', '.');
 	$('.results-lois-request-area').each(function() {
 		var a = $(this);
 		if (a.html().indexOf(record) === -1) {
 			a.remove(); // if any are visible, hide them. Prevents multiple from being open on single page. No use case fo it
 		}
 	});
 	var requestURL = 'https://lasiii.losrios.edu/search~S9?/' + record + '/' + record + '/1,1,1,B/request~' + record;
 	if ($('#resultListControl').html().indexOf('lois-request-' + record) === -1) {
 		var requestArea = $('<div style="display:none;" id="lois-request-' + record + '" class="results-lois-request-area" tabindex="0"><div class="results-lois-req-container"><div class="results-lois-req-controls"><a class="new-win" href="' + requestURL + '" target="_blank">Open in a new window</a><button id="remove-req-' + record + '" class="button close-request"  aria-role="close"  type="button">X</button></div><div class="request-overflow"><iframe style="display:none;" class="request-lois-iframe" width="100%" height="860" src="' + requestURL + '"></iframe></div></div></div>');
 		item.append(requestArea);
 		requestArea.find('iframe').on('load', function() {
 			$(this).show(1, function() {
 				requestArea.slideDown();
				$('#iframe-loading').remove();
 			});

 		});
 	}
 };
  var rtacCall = setInterval(function ()
  {
    if ($('.rtac-table').length)
    {
      clearInterval(rtacCall);
      $('.rtac-table').each(function() {
        _LR.fn.adjRTACRes($(this));
        //_LR.fn.condReqRes($(this).closest('.result-list-li'));
        var ftLink = $(this).closest('.display-info').find('.custom-link:contains(Full Text)');
        if (ftLink.length) {
         if ($(this).html().indexOf('ONLINE ACCESS') === -1) {
          ftLink.hide();
					}
				}
      });
	    	// get number of results
	var arr = $('.page-title').text().trim().split(' ');
	console.log(arr);
	var num = arr.pop();
 num = num.replace(',', ''); // comma breaks conversion to number
	num = parseInt(num, 10);
	console.log(num);
 if (!(_LR.smallScreen)) {
  if ((num < 10) && (num > 1) && (!($('.limiter[title~="Location"]').length)))
  {
																(function () {
																		var locs = ['arc', 'crc', 'flc', 'scc']; // will access _LR object for other stuff
																		var locationsList = (function () { // collect each location found in at least one table and put it in an array, so only those locations found in at least one record show as limiters
																			var arr = [];
																			$('.rtac-table').each(function () {
																				var a = $(this).text().toLowerCase();
																				for (var i = 0; i < locs.length; i++) {
																					var obj = _LR.colProps[i][locs[i]];
																					if (a.indexOf(obj.abbr) > -1) {
																						if (arr.indexOf(obj.colName) === -1) {
																							arr.push(obj.colName);
																						}

																					}
																				}

																			});
																			arr.sort(); // show them alphabetically. Would be better to count number of occurrences but more trouble than it's worth.

																			// build list
																			var output = '<ul class="rl-limiters" id="pseudo-locations">';
																			var locClass = 'pseudo-location';
																			for (var i = 0; i < arr.length; i++) {
																				output += '<li class="pseudo-loc-li">';
																				var elID = locClass + '-' + i;
																				output += '<input class="limControl ' + locClass + '" type="checkbox" data-col="' + arr[i] + '" id="' + elID + '">';
																				output += '<label class="limCaption ' + locClass + '" data-col="' + arr[i] + '" for="' + elID + '">' + arr[i] + '</label>';
																				output += '</li>';
																			}
																			output += '</ul>';
																			return output;

																		}());

																		// build list container
																		var markUp = '<div id="multiSelectCluster_LocationLibrary" class="multiSelectPanel">';
																		markUp += '<a class="rl-lim-heading collapse-toggle active">Location</a>';
																		markUp += locationsList;
																		markUp += '</div>';

																		$('#multiSelectClusterContainer').prepend(markUp); // this container exists even when it doesn't have anything in it
																		var updateCount = function () {
																			// scroll to top of list
																			$('html, body').animate({
																				scrollTop: ($('.page-title').offset().top)
																			}, 300);
																			// make number of results at top of page reflect currently visible records
																			setTimeout(function () { // if you set a delay, this runs before items are hidden
																				var total = $('.result-list-li:visible').length;
																				$('.page-title').html('1 - ' + total + ' of ' + total);
																			}, 800);
																		};
																		$('.pseudo-location').on('click', function () {

																			if ($(this).closest('li').find('input').prop('checked') === true) { // if was previously unchecked, this happens
																				$(this).closest('li').addClass('selected'); // will allow us to hide the others - making this binary simplifies things
																				var first = $(this).data('col').charAt(0); // get first letter in order to match 
																				console.log(first);

																				$('.result-list-li').each(function () {
																					var listItem = $(this);
																					var body = $(this).find('.rtac-table tbody');

																					body.find('tr').each(function () { // seems this extra loop is needed in order to catch all rows
																						if ($(this).find('td:first-child').html().trim().charAt(0) === first) {

																							listItem.addClass('pseudo-loc'); // adding this custom class will allow us to hide those items that lack it
																						}

																					});




																				});
																				$('.result-list-li').not('.pseudo-loc').fadeOut();
																				$('.record-index').hide();
																				$('.pseudo-loc-li').not('.selected').fadeOut('fast', function () {

																					updateCount();



																				});
																			} else { // if we are unclicking, need to show everything that was hidden and update the number shown at the top
																				$('.result-list-li').removeClass('pseudo-loc');
																				$('.pseudo-loc-li').removeClass('selected').show();
																				$('.result-list-li:hidden').fadeIn('fast', function () {
																					updateCount();
																				});
																			}


																		});
																	}());
   }

 }

      
    }

  }, 200);

});
jQuery('.result-list-li').on('click', '.close-request', function ()
{
  var a = jQuery('.results-lois-request-area');
  a.slideUp('slow', function() {
  a.remove();  
  });
  
});
//  }

console.log('rtac-adjust.js loaded');