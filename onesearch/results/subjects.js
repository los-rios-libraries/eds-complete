function subjSearchTerm() {
  var searchTerm = findSearchTerms();
  searchTerm = searchTerm.replace(/^(DE |AU |MM |SU |TX |TI |ZK )/, '');
  // searchTerm = searchTerm.replace(/"|(\-{2})/g, '');
  searchTerm = searchTerm.replace(/[\(\)"]/g, '');
  searchTerm = searchTerm.replace(/&amp;|&quot;/g, '');
  return searchTerm;
}

function isIE() {
  var ua = window.navigator.userAgent;
  if (ua.match(/MSIE |Trident\/|Edge\//) !== null) {
    return 'yes';
  } else {
    return 'no';
  }
}

function findSubjLink() {


  var custID = document.getElementById('custID');
  var custParam;
  if (custID) {
    custID = custID.innerHTML;
    custParam = 'custid=' + custID + '&';
  }
  var proxy = isItProxied();
  var catOnly = 'no';
  var catLimit = document.getElementById('common_FC');
  if (catLimit) {
    if (catLimit.checked === true) {
      catOnly = 'yes';
    }
  }
  var kw = subjSearchTerm();
  var url = 'http://scc.losrios.edu/library/tools/ebsco-scripts/subjects/index.php?proxy=' + proxy + '&' + custParam + 's=' + kw + '&catOnly=' + catOnly + '&subjbrowse';
  return url;




}

function subjPop(url, name) {
  var my_window;

  // screen.width means Desktop Width
  // screen.height means Desktop Height
  var w = screen.width / 3;
  var h;
  if (screen.height > 600) {

    h = screen.height / 1.5;
  } else {
    h = screen.height;
  }
  var right = (screen.width) - (w + 40);
  //  var center_left = (screen.width / 2) - (w / 2);
  // var center_top = (screen.height / 2) - (h / 2);
  var browser = isIE();
  if (browser === 'no') {


    my_window = window.open(url, name, "toolbar=yes,scrollbars=1, width=" + w + ", height=" + h + ", left=" + right + ", top=0");
    my_window.focus();
  } else {
    location.href = url;
  }
}


function getSubjects() {

  var subTerms = jQuery('.subjectResults');
  var subsRaw;
  var subText;
  if (!(subTerms.length)) {
    subText = '';
  } else {
    if (subTerms.length > 1) {
      subsRaw = subTerms.slice(0, 2);
    } else {
      subsRaw = subTerms;
    }
    subText = subsRaw.text();
    subText = subText.replace(/Subjects:/g, ' ');
    subText = subText.replace(/(\r\n|\n|\r)/gm, ' ');
    subText = subText.replace(/\s+/g,' ');
//    console.log(subText);
  }
  return subText;
}
getSubjects();

function createSubjiframe(el) {
    var kw = subjSearchTerm();
      if (kw.indexOf('on reserve') > -1) {
    jQuery('.related-info-area:contains("Subjects")').hide();
  } else {
    var search = getSubjects();
    if (search === '') {
        search = kw;
    }
console.log(search);

    var server = 'scc.losrios.edu/library/tools/ebsco-scripts/';
    var iframe = jQuery('<iframe />').attr({
      'id': 'subjWidget',
      'width': '100%',
      'height': '400',
      'src': 'http://' + server + 'subjects/widget.php?college=' + college + '&s=' + search
    }).css({
      'border': 'none'
    }).hide().appendTo(el);
    iframe.on('load', function() {
      jQuery(this).fadeIn();
    });

  }
}


console.log('subjects.js loaded');