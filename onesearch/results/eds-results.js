(function($) {
// miscellaneous adjustments for results screens
if (document.querySelector('.rtac'))
{
  console.log('page contains cat item');
  _LR.fn.loadScript('results/rtac-adjust', true);
}
// change electronic resources label, or others if need be
_LR.fn.renameLabel = function(wait, badLabel, longName, shortName) {
  setTimeout(function ()
  {
    $('.limCaption a:contains("' + badLabel + '")').text(longName);
    $('.record-icon .caption:contains("' + badLabel + '")').text(shortName).attr('title', longName);
    $('.limiter:contains("' + badLabel + '")').text(longName);
  }, wait);
};
_LR.fn.gvrl = {
  closeCO : function() {
  	$('.callout-list').slideUp('fast', function() {
  		$('.callout-list').remove();
  		ga('send', 'event', 'reference callout', 'close');
  	});
  },
  startCO: function(searchTerms) { // this is adapted from a prototype supplied by Eric Frierson at EBSCO, who envisioned it showing up somewhere further down the results list rather than replacing Research Starters
  	var resStartContainer = jQuery('.placard-container');
  	resStartContainer.hide();
  	// variables for us to set
  	var maxShown = 3;
  	// how many results before callout is displayed?                        
  	var locationToDisplay = 0;
  	//  var callout_type = 'list';
  	var exactMatch = 'y';
  	var sectionTitle = 'Topic Overviews';

  	// remove trailing s
  	//         searchTerms = searchTerms.replace(/s$/, '');
  	searchTerms = _LR.currentQuery.replace(/"/g, '');
  	console.log(searchTerms);
  	var searchQ = 'TI ' + searchTerms + ' NOT PZ critical essay NOT PZ viewpoint essay-Pro NOT PZ business case study NOT PZ Organization overview';
  	var edsapigateway_url = 'https://widgets.ebscohost.com/prod/encryptedkey/eds/eds.php?k=' + _LR.currentCol.edsGateway + '&s=0,1,1,0,0,0&q=search%3Fquery%3D' + encodeURIComponent(searchQ) + '%26searchmode%3Dall%26view%3Ddetailed%26resultsperpage%3D20%26pagenumber%3D1%26highlight%3Dn%26limiter%3DFT%3Ay';
  	console.log(edsapigateway_url);
  	var link;
  	jQuery.ajax({
  			type: 'GET',
  			url: edsapigateway_url,
  			dataType: 'json'
  		})
  		.done(function(jsonresponse) {
  			//     console.log('JSON Response: ');
  			//     console.log(jsonresponse);
  			if (parseInt(jsonresponse.SearchResult.Statistics.TotalHits, 10) > 0) {
  				var numshown = 0;
  				var source;
  				var rawtitle;
  				var summary;
  				var bookcallout = '<li class="result-list-li callout-list"><button type="button" aria-label="close" onclick="_LR.fn.gvrl.closeCO();">x</button><h3 class="section-title">' + sectionTitle + '</h3><ul id="bookcallout">';
  				jQuery(jsonresponse.SearchResult.Data.Records).each(function() {
  					var itemhtml = '<div class="callout_item">';
  					if (jQuery(this)[0].FullText.CustomLinks !== undefined) {
  						link = jQuery(this)[0].FullText.CustomLinks[0].Url;
  					} else {
  						link = this.PLink;
  					}
  					for (var i = 0; i < this.Items.length; i++) {
  						if (this.Items[i].Group == 'Ti') {
  							var title = $('<textarea />').html(this.Items[i].Data).text();
  							rawtitle = title;
  							titlepieces = title.split(' ');
  							if (titlepieces.length > 7) {
  								title = titlepieces.slice(0, 7).join(" ") + "...";
  							}
  							console.log(rawtitle);
  						}
  						if (this.Items[i].Group == "Src") {
  							source = $('<textarea />').html(this.Items[i].Data).text();
  						}
  						if (this.Items[i].Group == "Ab") {
  							summary = $('<textarea />').html(this.Items[i].Data).text();
  							summary = summary.replace(' [...]', '...');
  						}
  					}
  					var year = '';
  					var itemYear = this.RecordInfo.BibRecord.BibRelationships.IsPartOfRelationships[0].BibEntity.Dates[0].Y;
  					if (itemYear) {
  						year = ", " + itemYear;
  					}
  					var itemTitle = this.RecordInfo.BibRecord.BibRelationships.IsPartOfRelationships[0].BibEntity.Titles[0].TitleFull;
  					var fulltitle;
  					if (itemTitle) {
  						fulltitle = itemTitle;
  						fulltitle = fulltitle.replace(' :', ':');
  					} else {
  						fulltitle = '';
  					}
  					if (fulltitle.length > 0) {
  						fulltitle = fulltitle.replace('â€\'', '-');
  						source = '(<span class="callout-source-title">' + fulltitle + '</a></span><span class="callout-source-year">' + year + '</span>)';
  					}
  					var rawTitleLCTrim = rawtitle.trim().toLowerCase();
  					var searchTermsLC = searchTerms.toLowerCase();
  					if ((exactMatch === 'y') && (rawTitleLCTrim.indexOf(searchTermsLC) !== 0)) {
  						itemhtml = "";
  						//     console.log("Custom Callout eliminated \"" + rawtitle + "\" for non-match on \"" + searchTerms + "\"");
  					} else {
  						// gvrl descriptions start with the title - so eliminate that, but only first time
  						var rawTitleRegex = new RegExp('^' + rawtitle + ' ');
  						summary = summary.replace(rawTitleRegex, '');
  						summary = summary.replace(/^.*All rights reserved. /, '');
  						summary = summary.replace(/^.*([A-Z]){2}, USA /, '');
  						summary = summary.replace(/^(Definition|Glossary|Introduction) /, '');
  						itemhtml = '<li class="callout_list_item" ><div class="callout-title"><span class="callout-entry-title"><a href="' + link + '" target="_blank" onclick="ga(\'send\', \'event\', \'reference callout \', \'click \', \'item\')">' + rawtitle + '</a></span> <span class="callout_source">' + source + '</span></div><div class="callout_blurb">' + summary + ' <a class="callout-read-more" href="' + link + '" target="_blank" onclick="ga(\'send\', \'event\', \'reference callout \', \'click \', \'item\')">more</a></div></li>';
  						numshown = numshown + 1;
  					}
  					if (numshown <= maxShown) {
  						bookcallout += itemhtml;
  					}
  				});
  				bookcallout += '</ul><div id="callout-more-results"><a href="https://ezproxy.losrios.edu/login?url=https://go.galegroup.com/ps/i.do?dblist=GVRL&amp;st=T003&amp;qt=OQE~' + encodeURIComponent(searchTerms) + '&amp;sw=w&amp;ty=bs&amp;it=search&amp;p=GVRL&amp;s=RELEVANCE&amp;u=' + _LR.currentCol.galeID + '&amp;v=2.1" target="_blank" onclick="ga(\'send\', \'event\', \'reference callout\', \'click\', \'more results\')">Find more overviews in Gale eBooks</a></div></li>';

  				if (numshown > 0) {
  					console.log('numshown: ' + numshown);
  					if (locationToDisplay === 0) { // we are not currently using variations on this, so probably would be better to delete this conditional
  						jQuery('.result-list-li:nth-child(1)').hide().before(bookcallout).fadeIn();
  					} else {
  						jQuery('.result-list-li:nth-child(' + locationToDisplay + ')').hide().after(bookcallout).fadeIn();
  					}
  					ga('send', 'event', 'reference callout', 'show', searchTerms);
  					// research starters can take some time to show up, so need to wait a bit
  					var defaultImg = '<img src="' + _LR.servers.domain + _LR.servers.root + '/onesearch/results/gvrl-img/books-abs.jpg" alt="">';
  					var endImgSrch = setTimeout(function() { // might want to tighten this timeout and add a generic image when not found.
  						clearInterval(lookForImg);
  						console.log('image search ended');
  						clearInterval(pubCheck); // might want to move this
  						jQuery('.callout_list_item:nth-of-type(1)').addClass('has-img').prepend(defaultImg);
  					}, 4000);
  					var pubCheck = setInterval(function() {
  						if (!(jQuery('#PlacardTitle1_edspub').length)) {} else {
  							clearInterval(pubCheck);
  							console.log('found publication');
  							resStartContainer.attr('style', 'display:block !important;');
  						}

  					}, 100);
  					var lookForImg = setInterval(function()

  						{
  							if (!(jQuery('#PlacardImage1_ers').length)) {
  								if (jQuery('#PlacardTitle1_edspub').length) {
  									// because when there's a pub finder result, there's no research starter
  									clearInterval(lookForImg);
  									clearTimeout(endImgSrch);
  									jQuery('.callout_list_item:nth-of-type(1)').addClass('has-img').prepend(defaultImg);
  								}
  							} else {
  								var resStartImg = jQuery('#PlacardImage1_ers');
  								clearInterval(lookForImg);
  								clearTimeout(endImgSrch);
  								var resStartImgUrl = resStartImg.attr('src');
  								//            console.log(resStartImgUrl);
  								if (resStartImgUrl.indexOf('logors') === -1) {
  									//           resStartImg.css({'float': 'left', 'padding-right' :'10px'});
  									jQuery('.callout_list_item:nth-of-type(1)').addClass('has-img').prepend(resStartImg);
  								} else {
  									jQuery('.callout_list_item:nth-of-type(1)').addClass('has-img').prepend(defaultImg);
  								}
  							}
  						}, 50);
  				} else {
  					if (jQuery('.placard-container').length) {
  						resStartContainer.fadeIn();
  					} else {
  						var endPlacardWait = setTimeout(function() {
  							clearInterval(placardWait);

  						}, 5000);
  						var placardWait = setInterval(function() {
  							if (!resStartContainer) {

  							} else {
  								clearInterval(placardWait);
  								clearTimeout(endPlacardWait);
  								resStartContainer.fadeIn();
  							}
  						}, 50);
  					}
  				}
  			} else {

  				resStartContainer.fadeIn();

  			}
  		})
  		.fail(function(xhr, status, error) {
  			ga('send', 'event', 'reference callout', 'error', error); // for some reason errorThrown is not being sent
  			resStartContainer.show();
  		});


  }  
  };
  _LR.fn.replaceImage = function(a, url, isbn) {
   var pubType = a.find('.caption').text();
   if (/edskan|edsfod/.test(url)) { // for films on demand/kanopy
    var arr = url.split('.');
    if (arr[0] === 'edsfod') {
     url = 'https://fod.infobase.com/image/' + arr[1];
    }
    else if (arr[0] === 'edskan') { // this is causing some sort of error?
     url = 'https://www.kanopy.com/node/' + arr[1] + '/external-image';
    }
   }
  	$('body').append('<div id="bj-' + isbn + '" class="record-icon book-jacket" style="position:absolute;left:-9999px;"><img src="' + url + '" alt="Book Jacket"  ><p class="caption">' + pubType + '</p></div>');
  	var coverDiv = $('#bj-' + isbn);
  	coverDiv.find('img').on('load', function() { // make sure it's not a 1px dot
  		if ($(this).height() > 2) {
  			a.find('.record-icon').remove();
  			a.find('.display-info').prepend(coverDiv);
  			coverDiv.removeAttr('style').removeAttr('id');     
  		}

  	});

  };
}(jQuery));
_LR.fn.capAcronyms = function(el, subEl) {
    var abbr = /Arc|Crc|Flc|Scc/;
    if (abbr.test(el.text()) === true) { // only run the rest if the string is present
         // in select lists, label is the element, but need to allow for variation becasue of breadbox
        el.find(subEl || 'label').each(function() { 
            var text = jQuery(this).text().trim();
            if (abbr.test(text) === true) { // only run this on strings that include the regex
                var arr = text.split(' ');
                var acronym = arr.shift();
                if (abbr.test(acronym) === true) { // just to be sure
                    var newStr = acronym.toUpperCase() + ' ' + arr.join(' ');
                    jQuery(this).text(newStr);
                }
            }
        });
    }
};
 jQuery('.result-list-record').each(function(i) { // initiate oadoi search
  
          var a = jQuery(this);
          var db = _LR.fn.getResultItemData(a).db;
          if ((a.find('.pubtype-icon').attr('title') === 'Academic Journal') || (db === 'cmedm')) { // only run this for scholarly articles
			if ((((a.find('.pdf-ft').length) + (a.find('.html-ft').length)) === 0) || (_LR.guest())) {
			var count = 0;
			a.find('.ils-link').each(function() {
				if (/^full text/i.test(jQuery(this).text())) {
					count++;
				}
			});
			if ((count === 0 || _LR.guest())) {
				 _LR.fn.getDOI(_LR.fn.getResultItemData(a).db, _LR.fn.getResultItemData(a).an, a.find('.record-formats-wrapper'));
			}
			}

          }
          if (_LR.fn.getResultItemData(a).db.indexOf('cat0') === 0) { // get Open Library image as backup to Bowker
          	if (a.find('.pubtype-icon').length) {
          		if (_LR.fn.getResultItemData(a).url.indexOf('kanopy') > -1) {
          			var arr = _LR.fn.getResultItemData(a).url.split('su=');
          			var kanopyURL = arr.pop();
          			kanopyURL = kanopyURL.replace(/#\?/g, '');
             kanopyURL = kanopyURL.replace(/\/$/, ''); // remove trailing slash if it's there
          			var kanIDArr = kanopyURL.split('/');
          			var kanImg = 'https://www.kanopy.com/node/' + kanIDArr.pop() + '/external-image';
          			_LR.fn.replaceImage(a, kanImg, i);
          		} else {
          			_LR.fn.getResultItemData.isbn(a, function(isbn) {
          				var openLibImg = '//covers.openlibrary.org/b/isbn/' + isbn + '-M.jpg';
          				console.log('requesting ' + openLibImg);
          				_LR.fn.replaceImage(a, openLibImg, isbn);


          			});
          		}
          	}
          }
		  else if (a.find('.title-link').attr('href').indexOf('db=nmr') > -1) { // ftf links show for "web news" database - they will never be in ftf so just make those links work
			var ftfLink = a.find('.ils-link');
			if (ftfLink.length) {
				if (ftfLink.text().indexOf('Check for Full Text') > -1) {
					var link = a.find('.title-link').attr('href');
					ftfLink.attr('href', link);
				}
			}
			
			
		  }
    else if (_LR.fn.getResultItemData(a).db === 'edsart') {
     var el = a.find('.record-icon.pubtype');
     var url = el.data('book-jacket').ThumbImageUrl;
     el.prepend(_LR.fn.fixArtstorUrl(url));
     
     
    }
    else if (/edsfod|edskan/.test(_LR.fn.getResultItemData(a).db)) {
    
      _LR.fn.replaceImage(a, _LR.fn.getResultItemData(a).an, 'edsvid' + i);
      
    
    }
		});


(function ($)
{
  var showMoreSources = $('#multiSelectDocTypeContent .panelShowMore');
  var erNew = 'Archives, theses, grey literature, government docs, e-content';
  if ($('#common_FC').prop('checked') === false)
  { // if catalog-only is checked, Electronic resources means ebooks, unfortunately. So the archives thing doesn't really work.
    _LR.fn.renameLabel(300, 'Electronic', erNew, 'Archives, theses...');
    showMoreSources.on('click', function ()
    {
      _LR.fn.renameLabel(650, 'Electronic', erNew, 'Archives, theses...');
      /*
        setTimeout(function() {
           // this isn't working
                  $('.panelSortName').on('click', function() {
                _LR.fn.renameLabel(600, 'Electronic', erNew, 'Archives, theses...');
            });
            
        }, 300);
        */
    });
    // run gvrl check on appropriate pages
    if (_LR.custID.indexOf('scccatcomp') === -1) {
      if ($('.page-title').text().indexOf('Search Results: 1 -') > -1) {
        // don't run it if the query is long or uses a field code other than TI
        var sepKeywords = _LR.currentQuery.split(' ');
        var querysToExclude = new RegExp('(^(DE|SU|AU|AN|PT) |on reserve)');
        if ((sepKeywords.length < 6) && (querysToExclude.test(_LR.currentQuery) === false)) {
// check for extra limiters - don't show if they're there. Also make sure not limited by source type or database
          if (($('#breadboxContent .selected-limiters:nth-of-type(2) li').length <= 2) && (!($('.selected-limiters:nth-of-type(3)').length)) && ($('#_doc_type_ALL:checked').length) && ($('#_db_filter__all_dbs_:checked').length)) // note - length of selected limiters in breadbox includes hidden full text limiter. Should revisit these conditions and find better solution.
            {
              _LR.fn.gvrl.startCO(_LR.currentQuery);
            }
            else {
              console.log('no callout because of limiters on page');
            }

          }
          else {
            console.log('no callout because number of keywords is ' + sepKeywords.length + ' or because keywords are ' + _LR.currentQuery);
          }
        }
      }
      else {
        jQuery('.placard-container').show();
      }
  }
    
  $('.result-list-record').each(function ()
  {
    var dis = $(this);
    // truncate titles when very long
    var title = dis.find('.title-link');
    var titleText = title.text();
    if (titleText.length > 320)
    {
      var shortText = $.trim(titleText).substring(0, 300).split(" ").slice(0, -1).join(" ") + "...";
      title.text(shortText);
      // dis.append(children);
    }
  });
  $('.display-info').each(function ()
  {
    var a = $(this);
    children = a.children();
    mainText = a.clone().children().remove().end().text(); // see http://viralpatel.net/blogs/jquery-get-text-element-without-child-element/
    var shortText = $.trim(mainText).substring(0, 400).split(" ").slice(0, -1).join(" ") + "...";
    if (mainText.length > 600)
    {
      a.text(shortText);
      a.append(children);
    }
  });
_LR.fn.capAcronyms($('#multiSelectCluster_LocationLibraryContent')); // facet list
_LR.fn.capAcronyms($('.selected-limiters'), '.limiter'); // selected limiters in breadbox
$('#multiSelectCluster_LocationLibraryContent .panelShowMore').on('click', function() { // when you click "show more"
	var wait = setInterval(function() { // list is dynamically generated so need to wait for it to appear. EBSCO duplicates some ids so this took some trial and error
		if ($('#modalPanelForm .limCaption').length) {
			clearInterval(wait);
			_LR.fn.capAcronyms($('#modalPanelForm'));
		}
	}, 50);
});
if (_LR.guest()) { // for items EBSCO won't show to guest users, include title and adjust text (default text is obnoxious)
	$('.title-link').each(function() {
		var a = $(this);
		if (a.text().indexOf('Login to gain access') > -1) {
			a.text(a.attr('title')); // title attribute includes item title
			var link = $('.guest-login a').attr('href');
			a.closest('.result-list-record').find('.record-additional').html('Result cannot be displayed in guest mode. Please <a href="' + link + '">sign in</a> to view details.');
		}
	});
}
  $('.selected-limiters ul li a.remove').on('click', function ()
  {
    var breadboxText = $(this).next('span.limiter').text();
    ga('send', 'event', 'breadbox', 'remove', breadboxText);
  });
  $('#commonCheckboxLimiters input').on('click', function ()
  {
    var mainLimitsVal = $(this).prop('title');
    ga('send', 'event', 'Limit To', 'check', mainLimitsVal);
  });
  $('#commonCheckboxLimiters a').on('click', function ()
  {
    var mainLimitsText = $(this).text();
    ga('send', 'event', 'Limit To', 'click', mainLimitsText);
  });
  $('.ui-slider-handle').on('click', function ()
  {
    ga('send', 'event', 'Limit To', 'drag', 'date slider');
  });
  // FACETS!! Trying to catch them when the modal opens up is challenging...
  $('.multiSelectPanel ul li label a').on('click', function ()
  {
    var category = $(this).parents('.multiSelectPanel').children('a').text();
    var label = $(this).text();
    ga('send', 'event', category, 'click', label);
  });
  $('.multiSelectPanel a.collapse-toggle').on('click', function ()
  {
    var category = $(this).parents('.multiSelectPanel').children('a').text();
    var label = $(this).prop('title');
    ga('send', 'event', category, 'toggle', label);
  });
  $('.multiSelectPanel input').on('click', function ()
  {
    var category = $(this).parents('.multiSelectPanel').children('a').text();
    var label = $(this).parent().children('label').text();
    ga('send', 'event', category, 'check', label);
  });
  $('a.panelShowMore').on('click', function ()
  {
    var category = $(this).parents('.multiSelectPanel').children('a').text();
    var label = $(this).text();
    ga('send', 'event', category, 'click', label);
    setTimeout(function ()
    {
      $('.limCaption a').on('click', function ()
      {
        var category = $(this).parents('.modal-title').text();
        var label = $(this).text();
        ga('send', 'event', category, 'click', label);
      });
    }, 500);
  });
  $('#sortOptions a').on('click', function ()
  {
    var linkText = $(this).children('span:last-child').text();
    ga('send', 'event', 'sort results', 'select', linkText);
  });
  $('#pageOptions a').on('click', function ()
  {
    var category = $(this).attr('class');
    var linkText = $(this).children('span:last-child').text();
    ga('send', 'event', 'page options', 'select', category + ' - ' + linkText);
  });
  $('#addThis a').on('click', function ()
  {
    var linkText = $(this).prop('title');
    ga('send', 'event', 'Add This', 'select', linkText);
  });
  $('.refine-link').on('click', function ()
  {
    ga('send', 'event', 'results page', 'click', 'Refine Search');
  });
}(jQuery));
console.log('eds-results.js loaded');