﻿<option value="ACCT">Accounting (ACCT)</option>
<option value="ADMJ">Administration of Justice (ADMJ)</option>
<option value="AH">Allied Health (AH)</option>
<option value="ANTH">Anthropology (ANTH)</option>
<option value="ART">Art (ART)</option>
<option value="ARTH">Art History (ARTH)</option>
<option value="ASTR">Astronomy (ASTR)</option>
<option value="BIOL">Biology (BIOL)</option>
<option value="BUS">Business (BUS)</option>
<option value="BUSTEC">Business Technology (BUSTEC)</option>
<option value="CHEM">Chemistry (CHEM)</option>
<option value="CISA">Computer Info Science - Apps (CISA)</option>
<option value="CISC">Computer Info Science - Core (CISC)</option>
<option value="CISP">Computer Info Science - Program (CISP)</option>
<option value="COMM">Communication (COMM)</option>
<option value="DANCE">Dance (DANCE)</option>
<option value="ECE">Early Childhood Education (ECE)</option>
<option value="ECON">Economics (ECON)</option>
<option value="EMT">Emergency Medical Technician (EMT)</option>
<option value="ENGR">Engineering (ENGR)</option>
<option value="ENGCW">English Creative Writing (ENGCW)</option>
<option value="ENGED">English Education (ENGED)</option>
<option value="ENGLB">English Laboratory (ENGLB)</option>
<option value="ENGLT">English Literature (ENGLT)</option>
<option value="ENGRD">English Reading (ENGRD)</option>
<option value="ENGWR">English Writing (ENGWR)</option>
<option value="ESL">English as a Second Language (ESL)</option>
<option value="ESLG">ESL Grammar (ESLG)</option>
<option value="ESLL">ESL Listening (ESLL)</option>
<option value="ESLR">ESL Reading (ESLR)</option>
<option value="ESLW">ESL Writing (ESLW)</option>
<option value="ENVT">Environmental Technology (ENVT)</option>
<option value="FCS">Family and Consumer Science (FCS)</option>
<option value="FITNS">Fitness (FITNS)</option>
<option value="FREN">French (FREN)</option>
<option value="GEOG">Geography (GEOG)</option>
<option value="GEOL">Geology (GEOL)</option>
<option value="GERON">Gerontology (GERON)</option>
<option value="HEED">Health Education (HEED)</option>
<option value="HCD">Human Career Development (HCD)</option>
<option value="HIST">History (HIST)</option>
<option value="HUM">Humanities (HUM)</option>
<option value="KINES">Kinesiology & Athletics (KINES)</option>
<option value="LIBR">Library (LIBR)</option>
<option value="MATH">Mathematics (MATH)</option>
<option value="MEDTEC">Medical Technology (MEDTEC)</option>
<option value="MGMT">Management (MGMT)</option>
<option value="MKT">Marketing (MKT)</option>
<option value="MAKR">Modern Making (MAKR)</option>
<option value="MUFHL">Music Fundamentals, History & Literature (MUFHL)</option>
<option value="MUIVI">Instrumental/Voice Instruction (MUIVI)</option>
<option value="MUSM">Specializations in Music (MUSM)</option>
<option value="NUTRI">Nutrition and Foods (NUTRI)</option>
<option value="OTA">Occupational Therapy Assistant (OTA)</option>
<option value="PHIL">Philosophy (PHIL)</option>
<option value="PHYS">Physics (PHYS)</option>
<option value="POLS">Political Science (POLS)</option>
<option value="PRJMGT">Project Management (PRJMGT)</option>
<option value="PSYC">Psychology (PSYC)</option>
<option value="RE">Real Estate (RE)</option>
<option value="SILA">Sign Language Studies (SILA)</option>
<option value="SWHS">Social Work/Human Services (SWHS)</option>
<option value="SOC">Sociology (SOC)</option>
<option value="SPAN">Spanish (SPAN)</option>
<option value="SPORT">Sports (SPORT)</option>
<option value="STAT">Statistics (STAT)</option>
<option value="TA">Theatre Arts (TA)</option>
<option value="TAP">Theatre Arts Performance (TAP)</option>
<option value="VITI">Viticulture (VITI)</option>
<option value="WEXP">Work Experience (WEXP)</option>