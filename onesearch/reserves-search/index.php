<?php
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */
$http_origin = $_SERVER['HTTP_ORIGIN'];

 // if (strpos($http_origin, 'ebscohost') > -1)
// {  
    header('Access-Control-Allow-Origin: ' . $http_origin);
    header('Access-Control-Allow-Methods: GET, PUT, POST');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
// }
include '../shared/college-vars.php';
$collegeQuery = $college;
if ($college === 'flc') {
	$collegeQuery = '(flc OR edc)';
}
$textbooksLink;
switch ($college) {
	case 'arc': $textbooksLink = 'https://www.arc.losrios.edu/student-resources/library/textbooks';
		break;
	case 'scc': $textbooksLink = 'http://www.scc.losrios.edu/library/services/textbooks-on-reserve/#questions';
		break;
	case 'crc': $textbooksLink = 'https://www.crc.losrios.edu/student-resources/library/textbooks';
		break;
	case 'flc': $textbooksLink = 'https://www.flc.losrios.edu/student-resources/library/textbooks';
		break;
	default:
		$textbooksLink = '';
	
}
if (isset($_GET['framed'])) {
	$framed = $_GET['framed'];
}
if (isset($_GET['proxy'])) {
	$proxy = $_GET['proxy'];
}
if (isset($_GET['profile'])) {
	$profile = $_GET['profile'];
}
if ($proxy === 'true') {
	$urlBase = 'ezproxy.losrios.edu/login?url=https://search.ebscohost.com';
}
else {
	$urlBase = 'search.ebscohost.com';
}
if ($profile === 'scccatcomp') {
	$ebProfile = 'scccatcomp'; 
}
else {
	$ebProfile = 'eds';
}
?>
<!DOCTYPE html>
    <html>
    <head>
	<meta charset="utf-8">
		<meta name="robots" content="noindex">
	<title>Reserves search for EDS</title>
	<link href="res/style.css?0831" rel="stylesheet" type="text/css">
<?php
include '../shared/ga.php';
?>
    </head>

    <body data-college="<?php echo $college; ?>">
<!-- form will work with or without JavaScript -->
    <div id="reserves-search-form"> 
	<form  id="reservessearch"  name="reservessearch" target="_parent"  action="https://<?php echo $urlBase; ?>/login.aspx" data-college="<?php echo $collegeQuery; ?>">
		<input type="hidden" name="authtype" value="ip,guest">
		<input type="hidden" name="direct" value="true">
		<input type="hidden" name="custid" value="<?php echo $custid; ?>">
		<input type="hidden" name="site" value="eds-live">
		<input type="hidden" name="groupid" value="main">
		<input type="hidden" name="profile" value="<?php echo $ebProfile; ?>">
		<input type="hidden" name="cli0" value="FC">
		<input type="hidden" name="clv0" value="Y">
		<div class="column1">
		    <label for="Department">Department (e.g. Math) </label>
		    <select class="input-medium" id="Department" name="bquery" ><option selected="selected" value="">Select</option>
			<?php include ('departments-' . $college .'.php'); ?>
		    
		    </select>
		    </div>
		<div class="column1">
		    <label for="CourseNumber"  > Number (e.g. 300)
		    </label>
		    <input id="CourseNumber" name="bquery" placeholder="e.g. 300" title="Examples: 101, 300, 312. If you're not sure, leave it blank." type="text"  >
		    </div>
		
	    
		<div class="column1">
		    <label for="ProfessorsLastName">Professor's Last Name</label>
		    <input id="ProfessorsLastName" class="typeahead" name="bquery" placeholder="Last Name only" type="text" autocomplete="off" >
		</div>
		<div class="column1">
			<button id="show-kw-search" type="button">+ Add author or title</button>
		    <label for="TitleWords" id="TitleWordsLabel" title="Author's last name or book title, or leave it blank. NO EDITION NUMBERS! No typos!" style="display:none;" >Title or author <span> (optional)</span>
		    </label>
		    <input id="TitleWords" name="bquery" type="text" autocomplete="off" style="display:none;" >
		</div>
		<input type="hidden" name="bquery" value=" &quot;on reserve&quot;">
		<input type="hidden" name="bquery" value="<?php echo $collegeQuery; ?>">
	   <div id="submit-area">
 
		    <input  id="reservessubmit-<?php echo $college; ?>" class="reservessubmit button styled-button-c3 rs_preserve" type="submit" value="Find It"  >    
		</div>
	</form>
	<img src="//www.library.losrios.edu/resources/onesearch/reserves-search/res/loader.gif" id="loader" alt="loading" class="hidden">  
	<?php
	if ($textbooksLink !== '') {
		echo '<a id="need-help" href="' . $textbooksLink . '" target="_blank" onclick="ga(\'send\', \'event\', \'reserves form\', \'click\', \'help link\');">Need help finding textbooks?</a>';
		echo "\r\n";
	}
	?>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="res/jquery-ui.min.js"></script>
    

<script src="res/validate.js">

</script>
<script>
	(function() {
		var college = $('body').data('college');
		// load array of prof names for autocomplete - this is only used if loaded in iframe, if ajax call fails
		$.get('res/autocomplete.php?college=' + college)
			.done(function(data) {
				console.log('done');
				var arr = data.split(',');
				$('#ProfessorsLastName').autocomplete({
					source: function(request, response) {
						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
						response($.grep(arr, function(item) {
							return matcher.test(item);
						}));
					},
					autoFocus: true
				});
			})
			.fail(function(a, b, c) {
				ga('send', 'event', 'reserves form autocomplete', 'error', c);
			});
	}());
</script>
    </body>
    </html>
