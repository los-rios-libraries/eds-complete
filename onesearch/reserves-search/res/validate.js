if (typeof(_LR) !== 'object') {
	window._LR = {};
}
if (typeof(_LR.reserves) !== 'object') {
	_LR.reserves = {};
}

	jQuery('#show-kw-search').on('click', function() {
		var $ = jQuery;
		$(this).fadeOut('fast', function() {
			$(this).remove();
			$('#TitleWordsLabel, #TitleWords').css('display', 'block').fadeIn();
			$('#TitleWords').focus();
			});
			});
		
	if ($(window).width() < 500) {
   $('.reservessubmit').addClass('intip-basic-screen');
	}
	
	
_LR.reserves.showError = function(field, text, eventLabel)
{
 
  field.addClass('error').after('<div role="alert" class="errortext">' + text + '</div>').select();
  console.log('error type: ' + eventLabel);
  
};

_LR.reserves.reserveQValidator = function()
{
  var $ = jQuery;
  $('.errortext').remove();
  $('.error').removeClass('error');
  
  var numExp = /^[0-9]{2,3}([.][0-9]{1})?$/;
  var alphaExp = /^\s*[A-Za-z\-_\'\/]+$/;
  var courseNum = $('#CourseNumber');
  var profName = $('#ProfessorsLastName');
  var x = courseNum.val().trim();
  var y = profName.val().trim();
  var submitForm = true;
  if ((x !== '') && (numExp.test(x) === false))
  {
    this.showError(courseNum, 'The course number is usually 2 or 3 digits. Examples are 100, 300, and 310. If you’re not sure, leave it blank.', 'reserves - course number');
    submitForm = false;
  }
  if ((y !== '') && (alphaExp.test(y) === false))
  {
    this.showError(profName, 'Please type <em>only the last name</em>, with no spaces or punctuation. If you’re not sure, leave it blank.', 'reserves - prof names');
    submitForm = false;
  }
  if (submitForm === true)
  {
    var department = $('#Department');
var keywords = $('#TitleWords');
var resForm = $('#reservessearch');
var college = resForm.data('college');
var custid = resForm.find('input[name=custid]').val();
var profile = resForm.find('input[name=profile]').val();
var base = resForm.attr('action');

    var query = department.val() +' '+ x +' '+y +' '+ keywords.val() +' "on reserve" '+college;
    $('#loader').attr('class', 'shown');
	window.location = base + '?authtype=ip,guest&direct=true&custid='+custid+'&groupid=main&profile=' + profile + '&site=eds-live&cli0=FC&clv0=Y&bquery='+encodeURIComponent(query);
	console.log(query);
  }
};


jQuery( '#reservessearch' ).submit(function(event) {
  event.preventDefault();
_LR.reserves.reserveQValidator();

});

(function() { // transform dropdown as menu of buttons
	var $ = jQuery;
if (window === window.parent) { // in case reserves form is loaded as an iframe, don't activate all this since it will fail.
			if (!($('#dept-dialog').length)) { // don't recreate it if it already exists

			var output = '<div id="dept-dialog-container"><h2>All Departments:</h2><ul id="dept-dialog" >';
			$('#Department').find('option').each(function() {
				var a = $(this);
				var abbr = a.val(); // value of options is hte department abbreviation, used in keyword search
				var str = a.html(); // label is the text within the element
				output += '<li class="dept-options"><button type="button" class="dept-dialog button" data-abbr="' + abbr + '">' + str + '</button></li>'; // data-abbr attribute will be used to work with cookies

			});
			output += '</ul></div>';
			$(output).hide().appendTo('body');
			$('.dept-options:first-of-type button').text('Leave Blank'); // to provide an additional option for closing the dialog
		}
		if (!(_LR.smallScreen)) { // in april 2018 EBSCO launched responsive site--this is important for small screens
			$('#Department').on('click', function(e) {
				e.preventDefault();
				$('#Department option').hide(); // hide options elements; will show upon close
				var w = $(document).width() * 0.9; // for narrower screens, take up 90%
				if (w > 1000) {
					w = 1000;
				}


				$('#dept-dialog-container').dialog({
					'ui-dialog': 'department-menu',
					'width': w,
					'modal': true,
					'show': {
						duration: 100
					},
					'hide': {
						duration: 200
					},
					//	'title': 'Select a Department',
					'buttons': [{
						text: 'Close', // this shows at bottom of the dialog
						click: function() {
							$(this).dialog('close');
						}
					}],
					'open': function() {
						// recently used departments
						var recentDepts = _LR.fn.getCookie('reserveDepts'); // check cookie for presence of any recently selected depts
						if (recentDepts !== '') {
							// only show this All Departments heading if there are recent departments
							$('#dept-dialog-container h2').show();
							// add section to top for recent departments
							$('#dept-dialog-container').prepend('<div id="recentdept-section"><h2 id="recentdept">Recently Searched: </h2><ul id="recent-depts"></ul></div><hr>');
							var arr = recentDepts.split(',');
							if (arr.length > 4) { // in case there are more than 4 entries, remove the last one (this might duplicate some code below)
								arr.pop();
							}
							$('.dept-dialog').each(function() { // this checks the list for presence of each abbreviation in the cookie and copies it to the deparmtent list. Result is that they appear in alphabetical order rather than last-selected. Might not be ideal; couldn't easily figure out how to do it otherwise
								var code = $(this).data('abbr');
								if ($.inArray(code, arr) > -1) {
									$(this).parent().clone().appendTo($('#recent-depts'));
								}

							});

						} else {
							$('#dept-dialog-container h2').hide();
						}
						$('.ui-resizable-handle').hide(); // this is an EBSCO-provided dialog icon that looks awkward
						$('.ui-widget-overlay').on('click', function() { // close dialog if click outside of it
							$('#dept-dialog-container').dialog('close');

						});

					},
					'close': function() {
						$('#recentdept-section, #dept-dialog-container hr').remove(); // actually remove the div because it would need to be rebuilt with adjusted departmetns upon second click
						$('#Department option').show(); // restore dropdown
						$('#CourseNumber').focus(); // put cursor in next input box
					}
				});

				$('.dept-dialog').on('click', function() { // this is what happens when button is selected
					var abbr = $(this).data('abbr').trim(); // abbreviations are in data attribute
					if (abbr !== '') {
						$('#Department option[value="' + abbr + '"]').attr('selected', true); // switch selected attribute for matching option element

						$('#dept-dialog-container').dialog('close'); // close the dialog, triggering the close methods indicated above
						// add department to the cookie if it's not already there
						var setDepts = _LR.fn.getCookie('reserveDepts');
						if (setDepts === '') {
							_LR.fn.setCookie('reserveDepts', abbr, 1, _LR.domain);
						} else {
							var arr = setDepts.split(','); // abbreviations in cookie are separated by commas
							if ($.inArray(abbr, arr) === -1) { // if the chosen department is not already in the array, remove the last element and add it, then reset the cookie
								if (arr.length > 3) { // store maximum of 4 departments; so, if there are already 4, remove the last one
									arr.pop();
								}
								_LR.fn.setCookie('reserveDepts', abbr + ',' + arr, 1, _LR.domain); // add most recently selected department to the beginning of the list in the cookie
							}


						}
						if ($(this).parents('#recent-depts').length) { // if button is in recent departments list, send an event, to see how much this is being used
							ga('send', 'event', 'reserves form', 'click', 'recent department button');
						}
					} else {
						// this is if the first, Leave Blank button is clicked
						$('#dept-dialog-container').dialog('close');
						$('#Department option:first-of-type').attr('selected', true);
					}

				});


			});
		}	// load array of prof names for autocomplete
		if (_LR.currentCol.abbr !== 'crc') {
					$.get( _LR.servers.domain + _LR.servers.root + '/onesearch/reserves-search/res/autocomplete.php?college=' + _LR.currentCol.abbr)
		.done(function(data) {
			var arr = data.split(',');
			$('#ProfessorsLastName').autocomplete({
				source: function(request, response) {
					var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
					response($.grep(arr, function(item) {
						return matcher.test(item);
					}));
				},
				autoFocus: true
			});
		})
		.fail(function(a,b,c) {
			ga('send', 'event', 'reserves form autocomplete', 'error', c);
		});
		}

}
}());
