jQuery(function ($)
{
  var tags = [
"Abeyta","Abraham","Accooe","Aldrich","Alexander","Alford","Angove","Avila","Baltimore","Barthel","Bauer","Beatty","Beda","Bettencourt","Boarer-Pitchford","Bolton","Bonifacini","Bosner","Boylan","Bria","Brown","Buch","Bulaong","Burke","Cahoon","Cai","Carlson","Chambers","Chiang-Yamada","Chung","Ciampa","Clark","Clarke","Clayton","Collier","Considine","Cooper","Coronado-Barraza","Crennell","Cruz","Curran","Curtis","Daly","Daniels","Danner","Dao","Davatz","De_Mercurio","Desai","Deville","Diehl","Divittorio","Downey","Dyer","Eckman","Eiteneer","Eiteneer-Harmon","Eitener-Harmon","Emad","Emmert","Espinoza","Fannon","Faulkner","Fenyx","Fisher","Fitch","Fletcher","Flores","Foster","Fowler","Fox","Fuson","Gaffaney","Ghamami","Giordano","Graham","Green","Greene","Gregory","Hafezi","Hale","Harden","Harris","Hawley","Hay","Hayes","Haywood","Hegner","Heiler","Hensley","Herrmann","Hertzberg","Hicks","Hodge","Holzberg","Howerter","Hwang","Jacques","Jahangiri","Jazbi","Jenkins","Jensen","Johnson","Johnston","Jones","Jordan","Keller","Knudson","Kraemer","Krieg","Kroencke","Krohn","LACY","Lagala","LARRY-KEARNEY","Laurent","Leland","Lewis","Lopez","Mabanta","Mabry","Macias-Perez","Maddock","Mahdavi-Aghabeigi","Mahoney","Mailman","Malloy","Manfredi","Mason","McFaul","McGhee","McGhee-Pane","McQueen","Middleton","Miller","Mirmobiny","Mitchell","Mohrmann","Moraga","Moreno","Mowrer","Mullen","Nash-Rule","Negoda","Nelsenador","Nersesyan","Newnham","Nguyen","Nielsen","Nye","Oberth","Oliver","Olsen","Olts","Palomares","Patterson","Pearce","Pedro","Peralta","Pierce","Pietromonaco","Pinkerton","Pipkin","Pittman","Plaxton","Porter","Postiglione","Powell","Prelip","Price","Radding","Raines","Rauschkolb","Reed","Reese","Repetto","Ribaudo","Rinek","Ring","Ritzi-Marouf","Roberge","Roberts","Rodriguez","Roehr","Rogers","Ross","Royer","Rush","Sabu","Samarron","Samboceti","Samples","Sandholdt","Sapra","Sayago","Schmid","Schritter","Schunk","Siegfried","Silva-Henry","Simental","Skelly","Snow","Stanphill","Stark","Stevens","Suter","Swanson","Swithenbank","Taheri","Tandon","Tharalson","Tolopilo","Torrez","Trieu","Tully","Tyler","Van_Noord","Veras","Wada","Wai","Walker","Watanabe","Waterson","Wenneker","Wenzel","Wheeldon","Williams","Worth","Wright","Yeager","Zhang"
    ];
  $("#ProfessorsLastName").autocomplete(
  {
    source: function (request, response)
    {
      var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(tags, function (item)
      {
        return matcher.test(item);
      }));
    },
    autoFocus: true
  });
});