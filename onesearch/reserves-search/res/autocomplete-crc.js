jQuery(function ($)
{
  var tags = [
"Abeid","Abraham","Adams","Ahearn","Ahmadi","Aldredge","Allen","Amer","Andrews","Anzini-Varesio","Aptekar","Arden-Ogle","Baca","Bahm","Bahneman","Bass","Beloglovsky","Benevent","Benskin","Biesiadecki","Binder","Boeck","Bowles","Breitenbach","Brooks","Browne","Buck-Moyer","Bui","Burns","Bush","Butler","C._Torres","Camacho","Cann","Carinci","Carlisle","Carlson","Carmona","Carr","Carter","Castagna","Chapman","Chase","Cherry","Chow","Cinelli","Codella","Coelho","Coleman","Connally","Coughran","Crosier","Crowder","Dale","Dartez","Davenport","Degn","Deneke","Dimond","Do","Doan","Dobson","Donovan","Doyle","DuBray","Dunn","Dunne","Edman","Edmonds","Ellis","Emetarom","Erickson","Fagin","Falloon","Farnam","Feindert","Felkner","Ferry-Perata","Finnegan","Fishman","Fitz_Gibbon","Fong","Ford","Fortin","Fortunato","Fowler","Francisco","Frigm","Fuller","Gale","Garcia","Gee","Geissler","George","Giedd","Gilgun","Gill","Gilmore","Godinho","Gorman","Granquist","Greer","Gryffin","Guan","Gulati","Gunther","Hagenbuger","Hagenburger","Hancock","Hannan","Hansen","Hanson","Harding","Harrell","Harrington","Hee","Hicks","Hoang","Hodgkinson","Hoffman","Holden","Hom","Hooper","Howe","Huang","Huffman","Impinna","Inoue","Jackson","James","Jay-Anderson","Johnson","Kaufmann","Keightley","Kim","Knudsen","Korn","LaDue","Lam","Lane","Larsen","Larson","Lawlor","Le","Lee","Leung","Lewis","Limon","List","LoForte","Logan","Lopez","Lugo","Luu","Ly","Lynch","Mahmood","Mansourirad","Mapeso","Marchak","Markalanda","Marshall-Mills","Marslek","Martin","Mathis","Mayo","McCarthy","McCollum","McCoy","McDowell","McGee","McNee","Mederos","Meyers","Miller","Mills","Miranda","Mnuchowicz","Mojica","Monnot","Moore","Morales","Moreno","Morgan-Nance","Mulhern","Mullerworth","Munoz","Muranaka","Myren","Navarro","Neff","Nelsenador","Newman","Ngo","Nguyen","Nguyen-Vo","Nielson","Ninh","Noel","Ogilvie","Oliver","Oliver-Graybill","Osman","Otiono","Panagakos","Pandey","Pansius","Parilo","Parker","Parks","Paskey","Patterson","Pearles","Perales","Pereira","Peshkoff","Phan","Piner","Pollock","Posey","Post","Procsal","Quinn","Redmond","Reed","Reese","Reeves","Riese","Risenhoover","Robbins","Roberts","Rodrigues","Rodriguez","Rogers","Roltsch","Rushmore","Rusmore","Russell","Saake","Salmi","Samaniego","Sands-Pertel","Schreiner","Schroeder","Schulte","Seamons","Sertich","Sharkey","Shaver","Shih","Sigauke","Simpson","Smith","Sneed","Somadhi","Song","Soria_Martin","Spano","Speckman","Spisak","Staff","Stafford-Banks","Stassi","Steensland","Stewart","Strong","Sy","Tang","Taylor","Terry","Thao","Thomas-Fisk","Thunes","Torres","Trench","Trent","Tsuboi","Turner","Wagner","Wallace","Washington","Wassmer","Weinshilboum","Welkley","Wen","West","Whalen","Wheeler","Wheeler-Abeyta","Whitehead","Wiggins","Wildie","Williams","Williams_Brito","Williamson","Winter","Wise","Wunibald","Yarbrough","Young","Zaigralin","Zbierski","Zeng","Zisk"

    ];
  $("#ProfessorsLastName").autocomplete(
  {
    source: function (request, response)
    {
      var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(tags, function (item)
      {
        return matcher.test(item);
      }));
    },
    autoFocus: true
  });
});