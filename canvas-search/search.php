<?php
    session_start();
//    error_reporting(E_ALL);
//	ini_set('display_errors', '1');
	// allow use by different colleges
    $colleges = array(
    	'arc' => 'amerriv',
    	'crc' => 'cosum',
    	'flc' => 'ns015092',
    	'scc' => 'sacram'
    );
    $college = '';
    $custID = '';
    if (isset($_POST['custid'])) {
    	$custid = $_POST['custid']; // could just have people include custid as a form parameter
    }
    elseif(isset($_POST['college'])) {
        $college = $_POST['college'];
    	foreach($colleges as $key => $value) {
    		if ($key === $college) {
    			$custID = $value;
    			break;
    		}

    	}
    }
    
    $date = new DateTime();
    $timestamp = $date->getTimestamp(); // will compare to posted timestamp
    $formTimestamp = 0; // in case no timestamp is given on form
    if (isset($_POST['ts'])) {
        $formTimestamp = $_POST['ts'];        
    }
    $root = 'search.ebscohost.com';
    if (isset($_COOKIE['onesearchDomain'])) { // only used if authentication fails
    	if ($_COOKIE['onesearchDomain'] === 'proxy') {
    		$root = '0-search-ebscohost-com.lasiii.losrios.edu';
    	}
    }
	// stuff for ebsco permalink
    $keywords = '';
    if (isset($_POST['bquery'])) {
        $keywords = $_POST['bquery'];
    }
    $keywordsParam = '&bquery=' .$keywords;
    $direct = '&direct=true';
    $scopeSite = '&site=eds-live&scope=site';
	// patterns indicating search for databases
    $dbPatterns = '/ebsco( )?(host)?$|proquest|academic search complete|films on demand|cinahl|j( )?stor|lex[ui]s(( )?nex[iu]s)?|gale virtual|gvrl|^cq|onesearch|oxford art|^grove|artstor|ebooks|google scholar|business source|statista|opposing viewpoints|socindex|psycarticles|^eric$|education research complete|greenfile|intelecom|pubmed|medline|naxos|oxford english|oed|rcl|resources for college|science( )?direct|kanopy/';
    if ($keywords === '') { // some parameters are absent when keywords are not used
        $direct = '';
        $keywordsParam = '';
        $scopeSite = '';
    }
    if (preg_match($dbPatterns, strtolower($keywords))) {
        header('Location: https://www.library.losrios.edu/resources/databases/index.php?az&query=' . urlencode($keywords) . '&college=' . $college);
        exit();
        
    }
    $params = '&custid=sacram&groupid=main' . $keywordsParam . '&profile=eds';
    
    if (isset($_POST['session'])) {
        include_once('credentials.php'); // this file excluded from public repository - includes EBSCO user name and passwords for each college
        $user = getCredentials($credentials, $college, 'user');
        $password = getCredentials($credentials, $college, 'password');
        if (($_POST['session'] === session_id()) && (($timestamp - $formTimestamp) < (60 * 120))) { // in other words, session string sent as part of form must match session id on this page. Could be spoofed but that would take a good deal of work. Also, form sends a timestamp which must be within 2 hours of the search.
            header('Location: https://search.ebscohost.com/login.aspx?authtype=ip,uid&user=' . $user . '&password='. $password  . $params . $direct . $scopeSite);
            
            exit();
        }
        else {
            //echo 'caught else';
            header('Location: https://' . $root . '/login.aspx?authtype=ip,guest' . $params . $direct. $scopeSite);
            exit();
        }
    }
    else {
        // fallback if something fails! will be guest access if proxy cookie is not set. Also prevents people from getting stuck on this page if somehow they come here independently
		//echo 'final else';
        header('Location: https://' . $root . '/login.aspx?authtype=ip,guest' . $params . $direct. $scopeSite);
        exit();
    }
?>