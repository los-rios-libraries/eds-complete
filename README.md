# README #

This repository consists of the add-ons we use with EBSCO Discovery Service. We launched a version of EDS based on these scripts in August 2015.

## Basic Strategy ##

* Use bottom branding rather than results/detailed record widgets wherever possible
* losrios-bbV2.js is only script called in by EBSCOadmin bottom branding; others may be called in dynamically depending on type of content being viewed
* in a few cases, use iframes.
* original scripts are run on development profile; minimized scripts are used on production profile

## Notes ##

Much of this was written as I was learning Javascript, so to a more experienced eye many things will look odd. Also there are areas where jQuery is not used and should be; EBSCO does not load jQuery before bottom branding, so relying on it can be tricky. Eventually I figured out that the best way was to use setInterval to poll for its presence, and wait for it to be found before loading most of the scripts. I'm going through occasionally and rewriting things with jQuery, in order to make it less verbose/more readable and also more easily allow simple animations.

A few things are adaptations of scripts provided by EBSCO or found on the EDS Wiki.