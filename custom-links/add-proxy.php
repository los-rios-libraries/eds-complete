<?php
/*
// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
*/

if (isset($_GET['url'])) {
$url = $_GET['url'];
$proxy = 'ezproxy.losrios.edu';
$urlStrings = array(
                    'www.jstor.org',
                    'www.sciencedirect.com',
                    'hdl.handle.net', // acls humanities ebook
                    'www.oxfordartonline.com',
                    'sk.sagepub.com',
                    'fod.infobase.com',
                    'searchcenter.intelecomonline.net',
                    'artstor.org',
                    'dx.doi.org' // used in oxford art/grove
                    );
if (strpos($url, '.losrios.edu') === false) {
for ($i = 0; $i < count($urlStrings); $i++) {
    if (strpos($url, $urlStrings[$i]) !== false) {
        $proxiedURL = 'https://' . $proxy . '/login?url=' .$url;   
        if ($urlStrings[$i] === 'fod.infobase.com') {
            
            $proxiedURL = $proxiedURL . '&wID=' . $_GET['wID'];
        }
        header('Location: ' . $proxiedURL);
        exit;
    }
}
}
// sometimes when EDS is used with proxy, proxy may be included in custom link. FoD still needs some help
elseif (strpos($url, 'fod.infobase.com') !== false) {
    
    header('Location: ' . $url . '&wID=' . $_GET['wID']);
    exit;
    
}
else {
    
    header('Location: ' . $url);
    exit;
}
}
else {
    ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Redirect failed</title>
    <style>
    body {font-family:Arial,Verdana,sans-serif;}
    </style>
    <script>
  var gaID = 'UA-44798235-7';
  if (document.cookie.indexOf('lrGAOptOut=y') > -1) {
    window['ga-disable-' + gaID + ''] = true;
  }
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '' + gaID + '', 'auto');
  ga('set', 'anonymizeIp', true);
  ga('send', 'pageview');


</script>
    
</head>

<body>
<p>Something went wrong here! The error has been logged.</p>


</body>
</html>

 <?php
}
exit;
?>