<?php

// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);



$db = '';
$an = '';
// artstor
if (isset($_GET['db'])) {
    $db = $_GET['db'];
    
}
if ($db === 'artstor') {
    if (isset($_GET['an'])) {
    
        $an = $_GET['an'];
    }

    if (!empty($an)) {
        $arr = explode('.', $an);
        array_shift($arr);
        $artstorAn = implode('_', $arr);
 //       echo $artstorAn;
        $url = 'https://ezproxy.losrios.edu/login?url=https://library.artstor.org/asset/' . $artstorAn;
        header('Location: ' . $url);
        exit;
    }
}

elseif ($db === 'kanopy') {
    $urlBase = 'https://losrios.kanopystreaming.com/';
    if (isset($_GET['an'])) {
        $an = $_GET['an'];
    }
    $last = '';
    if (!empty($an)) {

        if (strpos($an, '/') !== false) {
            $arr = explode('/', $an);
            $last = array_pop($arr);
            $url = $urlBase . 'node/' . $last;
        
        }
        elseif (strpos($an, '.') !== false) {
            $arr = explode('.', $an);
            $last = array_pop($arr);
        }
        if ($last !== '') {
            $url = $urlBase . 'node/' . $last;
        }
        else {
            $url = $urlBase;
        }
        
    }
    else {
            $url = $urlBase;
        }
    header('Location: ' . $url);
    exit;
}
?>